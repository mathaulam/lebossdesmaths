from cours.models import Cours, Theme, Programme, Block, Grain, Lesson
from rest_framework import serializers


class LessonSerializer(serializers.ModelSerializer):

    id = serializers.ReadOnlyField()

    class Meta:
        model = Lesson
        fields = ['id', 'name']


class CoursSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Cours
        fields = ('id', 'nom', 'video', 'classe',
                  'processed', 'linked_epreuves')


class CoursMinimalSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Cours
        fields = ['id', 'nom']


class ThemeSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    fullname = serializers.SerializerMethodField()

    def get_fullname(self, obj):
        return u"%s > %s" % (obj.cours, obj.nom)

    class Meta:
        model = Theme
        fields = ('id', 'nom', 'fullname')


class ProgrammeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Programme
        fields = ('id', 'name')


class GrainSerializer(serializers.ModelSerializer):

    class Meta:
        model = Grain
        fields = ('id', 'name')


class BlockSerializer(serializers.ModelSerializer):

    class Meta:
        model = Block
        fields = ('id', 'name')