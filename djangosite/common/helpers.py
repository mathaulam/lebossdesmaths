from rest_framework.authtoken.models import Token

AUTH_HEADER = 'HTTP_AUTHORIZATION'


def check_admin_token(request):
    if AUTH_HEADER in request.META and request.META[AUTH_HEADER].startswith('Token '):
        token_str = request.META[AUTH_HEADER][6:]
        token_obj = Token.objects.get(key=token_str)
        return token_obj.user.is_staff or token_obj.user.is_superuser

    return False


def get_token_user(request):
    if AUTH_HEADER in request.META and request.META[AUTH_HEADER].startswith('Token '):
        token_str = request.META[AUTH_HEADER][6:]
        token_obj = Token.objects.get(key=token_str)
        return token_obj.user
    return None
