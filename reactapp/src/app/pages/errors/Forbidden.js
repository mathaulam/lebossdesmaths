import React from "react";
import TopNavBar from "app/modules/topbar/TopNavBar";
import BasePage from "app/pages/BasePage";
import "app/pages/pages.css";

class Forbidden extends BasePage {
    
    render() {
            return (
                <div>
                    <TopNavBar />
                    <div className="container">
                        <div className="jumbotron">
                            <h2>Authentification requise</h2>
                            <hr />
                            <p>Merci de vous authentifier en utilisant le formulaire de login situé dans le menu.</p>
                        </div>
                    </div>
                </div>
            );
        }

}

export default Forbidden;