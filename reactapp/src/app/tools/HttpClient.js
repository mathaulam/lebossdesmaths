import axios from "axios";
import {appConfig} from "app/tools/AppConfig";

class HttpClient {

    get(path, successCb, errorCb) {
        let url = appConfig.getServerUrl() + path;
        let config = {
            headers: {}
        };

        if (localStorage.token) {
            config.headers["Authorization"] = "Token " + localStorage.token;
        }

        axios.get(url, config)
            .then(successCb)
            .catch(errorCb);
    }

    post(path, data, successCb, errorCb) {
        let url = appConfig.getServerUrl() + path;
        let config = {
            headers: {}
        };

        if (localStorage.token) {
            config.headers["Authorization"] = "Token " + localStorage.token;
        }

        axios.post(url, data, config)
            .then(successCb)
            .catch(errorCb);
    }
}

export let httpClient = new HttpClient();
