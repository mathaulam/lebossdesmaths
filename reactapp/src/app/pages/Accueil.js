import React from "react";
import {Link} from 'react-router';
import BasePage from "./BasePage";
import {httpClient} from 'app/tools/HttpClient';
import TopNavBar from "app/modules/topbar/TopNavBar";
import {preprocessHtml, getErrorMsg} from 'app/tools/Helpers.js';
import {auth} from 'app/tools/Auth';
import "app/pages/pages.css";

class Accueil extends BasePage {

    constructor(props) {
        super(props);
        this.state = {homepage: '', exams: '', coursList: '', grains: []};
        this.loadData = this.loadData.bind(this);
        this.loadData();

    }

    loadData() {

        if(auth.getUserData() && auth.getUserData().profile.classe){
            // Get list of lessons.
            httpClient.get(
                "/api/classes/" + auth.getUserData().profile.classe.id + "/cours_full",
                (resp) => {
                    this.setState({coursList: resp.data})
                    // Get list of grains related to each lesson.
                    for (let j = 1; j < this.state.coursList.length; j++) {
                         httpClient.get(
                            "/api/lessons/" + this.state.coursList[j].id + "/grains",
                            (resp) => {
                                this.setState({ obj: resp.data })
                            },
                            (error) => alert(getErrorMsg(error))
                        );
                    }
                },
                (error) => alert(getErrorMsg(error))
            );



            Object.keys(this.state.coursList).map( (key) => {
                httpClient.get(
                    "/api/lessons/" + this.state.coursList[key].id + "/grains",
                    (resp) => {
                        obj = grains[this.state.coursList[key].id]
                        this.setState({ obj: resp.data })
                    },
                    (error) => alert(getErrorMsg(error))
                );
            });
        }
        else{
            let homepage_url = "/api/homepage/";
            let exams_url = "/api/sublanding/minimal";
            // Get paragraphs of homepage.
            httpClient.get(
                homepage_url,
                (resp) => this.setState({homepage: resp.data['paragraphs'], validating: false}),
                (error) => alert(getErrorMsg(error))
            );
            // Get a list of exams
            httpClient.get(
                exams_url,
                (resp) => this.setState({exams: resp.data, validating: false}),
                (error) => alert(getErrorMsg(error))
            );
        }

    }
    renderBody() {
        return (
            <section className="title-section">
                <div className="container">
                    <div className="row">
                        {this.props.location.query.logout &&
                            <div className="alert alert-info"><b>Deconnexion réussie.</b> A Bientôt !</div>
                        }
                        {
                            auth.getUserData() && auth.getUserData().profile.classe &&
                                <div className="col-sm-6 vcenter">
                                    <div className="slide-text">
                                        <h1>page personnelle de { auth.getUserData().username }</h1>
                                        <ul>
                                        {
                                            this.state.coursList.map((object, i) => {
                                                return (
                                                    <h2>
                                                    <li key={object.id}>
                                                        <Link to={ '/cours/' + object.id}>{object.nom}</Link>
                                                        <ul>
                                                            {
                                                                object.linked_epreuves.map((epr, i) => {
                                                                    return (
                                                                        <li key={i}><h3><Link to={ '/epreuves/' + epr}>Epreuve</Link></h3></li>
                                                                    )
                                                                })
                                                            }

                                                        </ul>

                                                    </li>
                                                    </h2>
                                                )
                                            })
                                        }
                                        </ul>
                                    </div>
                                </div>
                        }
                        {
                            auth.getUserData() && !auth.getUserData().profile.classe &&
                                <div className="col-sm-6 vcenter">
                                    <div className="slide-text">
                                        <h1>Welcome { auth.getUserData().username }</h1>
                                        <h2>You are a programme-based user.</h2>
                                    </div>
                                </div>
                        }
                        {
                            !auth.getUserData() &&
                                <div className="col-sm-6 vcenter">
                                    <div className="slide-text">
                                        {
                                             Object.keys(this.state.homepage).map((paragraph) => {
                                                return (
                                                    <div className="homepage_paragraph" key={paragraph}>
                                                        <h2>{this.state.homepage[paragraph].title}</h2>
                                                        <p>{this.state.homepage[paragraph].content}</p>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                </div>
                        }
                        <div className="col-sm-6 vcenter">                            
                            <img 
                                id="prof-tableau"
                                src="/img/illustrations/maitresse_tableau.png" 
                                className="img-responsive illustration" 
                                alt="Prof de Maths" />
                        </div>
                    </div>
                    {
                        !auth.getUserData() &&
                            <div className="row">
                                {
                                     Object.keys(this.state.exams).map((exam) => {
                                        return (
                                            <div className="col-md-4" key={exam}>
                                                <div className="panel panel-default">
                                                  <div className="panel-heading">
                                                    <h3 className="panel-title">{this.state.exams[exam].name}</h3>
                                                  </div>
                                                  <div className="panel-body">
                                                    for more details about {this.state.exams[exam].name}, please click
                                                      <Link to={ '/sublanding/' + this.state.exams[exam].id}> here</Link>
                                                  </div>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                    }

                </div>
            </section>
        );
    }
}

export default Accueil;