from django.http import JsonResponse, HttpResponseBadRequest
from django.shortcuts import render
from django.conf import settings

from rest_framework import generics, viewsets
from rest_framework.decorators import list_route
from rest_framework.views import APIView
from rest_framework.response import Response

from landing_pages.serializers import HomepageSerializer, SublandingListSerializer
from .models import Homepage, SublandingPage


class HomepageView(APIView):
    """
    End-point for retrieving the homepage settings.
    """

    def get_queryset(self):
        """
        Returns the details for current site's homepage info.
        """
        try:
            site_id = settings.SITE_ID
        except AttributeError:
            site_id = 1
        return Homepage.objects.filter(sites=site_id)

    def get(self, request, format=None):
        """
        Handling the GET request.
        """
        try:
            homepage = self.get_queryset().first()
            print(self.get_queryset())
            serialized_homepage = HomepageSerializer(homepage)
            return Response(data=serialized_homepage.data, status=200)

        except Exception, error:
            response = "Erreur: %s" % str(error)
            return HttpResponseBadRequest(response)


class SublandingViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows cours to be viewed.
    """
    queryset = SublandingPage.objects.all()
    serializer_class = SublandingListSerializer

    @list_route()
    def minimal(self, request, *args, **kwargs):
        sublanding = SublandingPage.objects.all()
        serializer = SublandingListSerializer(sublanding, many=True, context={'request': request})
        return Response(serializer.data)
