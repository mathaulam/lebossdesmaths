from __future__ import unicode_literals

from django.apps import AppConfig


class LandingPagesConfig(AppConfig):
    name = 'landing_pages'
