from qcms.models import Qcm, QcmSequence
from cours.serializers import ThemeSerializer
from rest_framework import serializers


class QcmSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Qcm
        fields = '__all__'


class QcmSequenceSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    theme = ThemeSerializer()

    class Meta:
        model = QcmSequence
        fields = ['id', 'theme', 'score', 'created',
                  'updated', 'expiration']
