import React from "react";
import BasePage from "./BasePage";
import TopNavBar from "app/modules/topbar/TopNavBar";
import {Link} from 'react-router';
import {auth} from 'app/tools/Auth';
import {httpClient} from 'app/tools/HttpClient';
import {getErrorMsg} from 'app/tools/Helpers.js';
import "app/pages/pages.css";

class EleveMedailles extends BasePage {

    constructor(props) {
        super(props);
        this.minPartMonth = 50;
        this.state = {partGlobal: -1, partMonth: -1, medailles: null};
        this.loadMedailles = this.loadMedailles.bind(this);
        this.renderMedaillesList = this.renderMedaillesList.bind(this);
        this.loadMedailles();
    }

    loadMedailles() {
        // Get name of classe
        httpClient.get(
            "/api/medailles",
            (resp) => {
                if (resp.data.partGlobal) {
                    this.setState({partGlobal: parseFloat(resp.data.partGlobal)});
                }
                if (resp.data.partMonth) {
                    this.setState({partMonth: parseFloat(resp.data.partMonth)});
                }
                if (resp.data.medailles) {
                    this.setState({medailles: resp.data.medailles});
                }
            },
            (error) => alert(getErrorMsg(error))
        );
    }

    renderMedaillesList() {
        if (this.state.medailles.length == 0) {
            return;
        }

        let coursList = [];

        // Reorganize data as a list of list of obj
        for (let coursName of Object.keys(this.state.medailles)) {
            let themeList = [];
            for (let themeName of Object.keys(this.state.medailles[coursName])) {
                let theme = {
                    name: themeName,
                    medal: this.state.medailles[coursName][themeName]
                };
                themeList.push(theme);
            }
            let cours = {
                name: coursName,
                themes: themeList
            }
            coursList.push(cours);
        }
        
        return (
            <div>
                {coursList.map((cours, i) => {
                    return (
                        <div key={cours.name}>
                            <hr/>
                            <h2>{cours.name}</h2>
                            <table className="medal-table">
                                <tbody>
                                {cours.themes.map((theme, i) => {
                                    return (
                                        <tr key={cours.name + theme.name}>
                                            <td>{theme.name}</td>
                                            <td>
                                            {theme.medal == 0 && <img src="/img/medailles/rien.png" alt="Rien" title="Rien" />}
                                            {theme.medal == 1 && <img src="/img/medailles/bronze.png" alt="Bronze" title="Bronze" /> }
                                            {theme.medal == 2 && <img src="/img/medailles/argent.png" alt="Argent" title="Argent" />}
                                            {theme.medal == 3 && <img src="/img/medailles/or.png" alt="Or" title="Or" />}
                                            {theme.medal == 4 && <img src="/img/medailles/diamant.png" alt="Diamant" title="Diamant" />}
                                            </td>
                                        </tr>
                                    )
                                })}
                                </tbody>
                            </table>
                        </div>
                    )
                })}
            </div>
        )
    }

    renderBody() {

        if (this.state.medailles == null) {
            return this.showLoader();
        }

        return (
            <div>
                <section className="title-section">
                    <div className="container">
                        <div className="row">
                            <h1>Mes Médailles</h1>
                        </div>
                    </div>
                </section>
                <div className="container">
                    {this.state.partMonth > -1 &&
                    <div>
                        <small>
                            Taux de participation global: <b>{this.state.partGlobal}% </b>
                            <br />
                            Taux de participation depuis 30 jours: <b>{this.state.partMonth}% </b>
                        </small>
                    </div>
                    }
                    {this.state.partMonth >= this.minPartMonth ?
                    <div className="medailles-list-wrapper">
                        {this.renderMedaillesList()}
                    </div>
                    :
                    <div>
                        Vous n'avez pas accès aux médailles car votre taux de participation sur les derniers 30 jours
                        est inférieur à {this.minPartMonth}%.
                    </div>
                    }
                </div>
            </div>
        );
    }
}

export default EleveMedailles;