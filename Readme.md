# A. PRE-REQUIS

# 1. Initialisation de la BDD

Créer une base données MySQL `bossdesmaths_db` puis après avoir effectué les migrations Django, s'assurer que les tables utilisent le moteur InnoDB.
La commande suivante permet de générer une liste de commandes SQL pour convertir les tables en InnoDB:

    SELECT CONCAT('ALTER TABLE `', table_name, '` ENGINE=InnoDB;') AS sql_statements FROM information_schema.tables AS tb WHERE table_schema = 'lebossdesmaths' AND `ENGINE` = 'MyISAM' AND `TABLE_TYPE` = 'BASE TABLE' ORDER BY table_name DESC; 

Le moteur InnoDB est nécessaire pour supporter les transactions.

# 2. Accès AWS

- Instance RDS: mathaulam/labossdesmaths
- Compte IAM "dev": dev/devmaths (authentification sur https://297769076484.signin.aws.amazon.com/console)
- Clés d'accès "dev": AKIAIWTL4GYVE3F4GCMA / iAlxu3p8u5GgtW4oxIin4mSMHJz6DcGnlcgOuikm (si expiré, demander à Anthony d'en regénérer)

Que ce soit depuis votre local (pour upload l'image) ou depuis l'instance EC2 sur AWS (pour lancer un container) il faut s'authentifier avec docker auprès du référentiel docker AWS:

```
$ aws configure
<inserer cles publique et privee dev>

$ aws ecr get-login --no-include-email --region eu-west-1 | bash
```

# B. EN LOCAL

## 1. Mode Dev (Sans Docker)

Lancer le back-end et le front-end séparément et en dehors des conteneurs docker:

Back-End:

- Installer python 2.7 puis installer les dependances en faisant `pip install -r config/requirements.txt`

- Pour lancer le backend, aller dans `djangosite` puis lancer `export DEBUG=true; python manage.py runserver`

Front-End:

- Installer nodejs v6.9+ et npm v3.10+ et lancer `npm install`

- Pour lancer le front-end avec le backend local Django, aller dans `reactapp` puis lancer `npm start`

- Pour lancer le Front-End en utilisant un faux backend "mock" au lieu de Django, lancer `npm run dev`.
Ceci renvoie des contnenus statiques (définis dans `reactapp/rest-mock.json`) afin de pouvoir afficher les pages avec des fausses données.

## 2. Construire l'image Docker et tester les conteneurs

Aller à la racine du projet où se trouve le fichier `docker-compose.yml`.

Dans `docker-compose.yml` remplacer `image: <chemin_vers_image_prod>` par `build: .` (normalement déjà existant en commentaire).
    
Ensuite exécuter la commande suivante, qui va construire l'image Docker `lebossdesmaths_web` en local, puis le lancer avec son conteneur MySQL associé:

```
$ docker-compose up --build -d
```

## 3. Sauvegarder l'image docker vers le referentiel AWS

S'authentifier, tagger l'image docker puis envoyer:

```
$ aws ecr get-login --no-include-email --region eu-west-1 | bash
$ docker tag lebossdesmaths_web:latest 297769076484.dkr.ecr.eu-west-1.amazonaws.com/lebossdesmaths_web:latest
$ docker push 297769076484.dkr.ecr.eu-west-1.amazonaws.com/lebossdesmaths_web:latest
```

# C. EN PRODUCTION

## 1. Lancer les conteneurs sur l'intance EC2

Pour le logguer sur l'instance EC2 il faut utiliser la cle ssh `ec2-ssh.pem`.

```
$ ssh -i ec2-ssh.pem ubuntu@ec2-52-16-189-12.eu-west-1.compute.amazonaws.com
```

Ensuite s'authentifier avec docker, supprimer le conteneur et l'image existants, puis relancer les conteneurs avec docker-compose en se positionant dans le bon dossier:

```
$ aws ecr get-login --no-include-email --region eu-west-1 | bash
$ docker rm -f lebossdesmaths_web_1
$ docker rmi 297769076484.dkr.ecr.eu-west-1.amazonaws.com/lebossdesmaths_web:latest

$ cd /home/ubuntu/lebossdesmaths
$ docker-compose up -d
```

## 2. Comment debugger

Il est possible de lancer un conteneur PhpMyAdmin pour investiguer la BDD, par exemple sur le port 8080:

```
$ docker run --name phpmyadmin -p 8080:80 --network=lebossdesmaths_app_net -e PMA_HOST=db -d phpmyadmin/phpmyadmin
```

Vous pouvez aussi ouvrir une session bash dans le container, si besoin d'investiguer le filesystem et lire les logs:

```
$ docker exec -ti lebossdesmaths_web_1 bash

root# tail /var/log/django.log
root# tail /var/log/nginx/error.log
root# tail /var/log/nginx/access.log
root# tail /var/log/uwsgi/djangoapp.log
root# tail /var/log/uwsgi/emperor.log
```

## 3. Volumes Dockers

Les répertoires suivants sont branchés sur les conteneurs, ils ne sont jamais supprimés.
Cela permet aux données dans ces répertoires d'être persistés lorsque l'on renouvelle les conteneurs.

- `/var/lib/docker/volumes/lebossdesmaths_log_data`: Contient les fichiers de logs (destination de `/var/log` dans le conteneur web).
- `/var/lib/docker/volumes/lebossdesmaths_media_data`: Contient les fichiers de média uploadés (images dans les cours par exemple) dans le conteneur web.
- `/var/lib/docker/volumes/lebossdesmaths_mysql_data`: Contient les données MySQL utilisés par le conteneur db.
