FROM python:2.7-slim

# Install all debian packages
RUN apt-get update && apt-get install -y \
		apt-utils \
		gcc \
		curl \
		mysql-client default-libmysqlclient-dev \
		sqlite3 \
		vim net-tools \
		supervisor \
		nginx \
	--no-install-recommends && rm -rf /var/lib/apt/lists/*

# Install node (needs curl, installed in previous command)
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash &&\
		apt-get install -y nodejs

# Install all python dependency libs
COPY config/requirements.txt /app/
RUN pip install -r /app/requirements.txt

# Copy all source files
COPY . /app/

# Install npm dependencies in react source code
WORKDIR /app/reactapp/
RUN npm install
WORKDIR /app/

# Run the script which compiles bundles using webpack, then sends the bundles
# to the django project as static files, and finally runs collectstatic
RUN bash compile-react-django.sh

# After the bundles have been generated, the react source code is not needed anymore
RUN rm -rf reactapp/

# Configure Nginx, uWSGI and supervisord
RUN ( cp config/nginx.conf /etc/nginx/nginx.conf &&\
	mkdir /var/log/uwsgi &&\
	ln -s config/supervisord.conf /etc/supervisor/conf.d/)

# Exposed port
EXPOSE 80

CMD ["/usr/bin/supervisord", "-c", "/app/config/supervisord.conf"]