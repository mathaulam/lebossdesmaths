from __future__ import unicode_literals

from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models

from preferences.models import Preferences


class Homepage(Preferences):
    """
    A model class that represents the components of homepage.
    """
    class Meta:
        verbose_name_plural = 'Homepage settings'

    paragraphs = models.ManyToManyField(
        to='landing_pages.Paragraph',
        related_name='related_homepage',
    )

    # Methods
    def __str__(self):
        return 'Homepage settings for site: ' + str(self.sites.first())


class Email(Preferences):
    """
    A model that handles the settings of email messages.
    """
    class Meta:
        verbose_name = 'Email Settings'
        verbose_name_plural = 'Email Settings'

    alert_email1 = RichTextUploadingField(
        blank=True,
        verbose_name='Alert email type 1 message',
        help_text='{{ first_name }} to insert the user first name, '
                  '{{ last_name }} to insert the user last name, '
                  '{{ days }} to insert number of days without activity.'
    )
    alert_email2 = RichTextUploadingField(
        blank=True,
        verbose_name='Alert email type 2 message',
        help_text='{{ first_name }} to insert the user first name, '
                  '{{ last_name }} to insert the user last name, '
                  '{{ days }} to insert number of days without activity.'
    )
    link_email = RichTextUploadingField(
        blank=True,
        verbose_name='Link email message',
        help_text='{{ link }} to insert the lesson link, '
    )


class Paragraph(models.Model):
    """
    A model that represents the informative paragraph.
    """
    title = models.CharField(max_length=220)
    content = models.TextField()
    main_image = models.ImageField(null=True, blank=True)
    link = models.URLField(null=True, blank=True)

    # Methods
    def __str__(self):
        return self.title


class SublandingPage(models.Model):
    """
    A model that represents the sublanding page.
    """
    name = models.CharField(max_length=220)
    paragraphs = models.ManyToManyField(
        to='landing_pages.Paragraph',
        related_name='related_page',
    )
    related_class = models.ForeignKey(
        to='classes.Classe',
        related_name='offers',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name='Classe'
    )
    related_programme = models.ForeignKey(
        to='cours.Programme',
        related_name='offers',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name='Programme'
    )

    # Methods
    def __str__(self):
        return self.name
