import React from "react";
import {httpClient} from 'app/tools/HttpClient';
import {appConfig} from "app/tools/AppConfig";
import  ReactDOM  from 'react-dom';
import {getErrorMsg} from "../../tools/Helpers";

class RegistrationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: "",
      last_name: "",
      username: "",
      email: "",
      email_confirm: "",
      password: "",
      password_confirm: "",
      plan_list: "",
      plan: "",
      block_list: "",
      block: "",
      exam: "",
      class_id: "",
      programme_id: ""
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFnameChange = this.handleFnameChange.bind(this);
    this.handleLnameChange = this.handleLnameChange.bind(this);
    this.handleUserNameChange = this.handleUserNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleEmailConfirmChange = this.handleEmailConfirmChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handlePasswordConfirmChange = this.handlePasswordConfirmChange.bind(this);
    this.handlePlanChange = this.handlePlanChange.bind(this);
    this.handleBlockChange = this.handleBlockChange.bind(this);
    this.loadData = this.loadData.bind(this);
    this.loadData();
  }

  loadData() {
    let plans_url = '/api/programmes';
    let blocks_url = '/api/programmes/'+this.props.programme_id+'/blocs';
    // Get available programmes.
    httpClient.get(
        plans_url,
        (resp) => {
          this.setState({plan_list: resp.data});
        },
        (error) => alert(getErrorMsg(error))
    );

    // Get available blocks.
    httpClient.get(
        blocks_url,
        (resp) => {
          this.setState({block_list: resp.data});
        },
        (error) => alert(getErrorMsg(error))
    );
  }

  handleFnameChange(event) {
    this.setState({ first_name: event.target.value });
  }

  handleLnameChange(event) {
    this.setState({ last_name: event.target.value });
  }

  handleUserNameChange(event) {
    this.setState({ username: event.target.value });
  }

  handleEmailChange(event) {
    this.setState({ email: event.target.value });
  }

  handleEmailConfirmChange(event) {
    this.setState({ email_confirm: event.target.value });
  }

  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  handlePasswordConfirmChange(event) {
    this.setState({ password_confirm: event.target.value });
  }

  handlePlanChange(event) {
    this.setState({ plan: event.target.value });
  }

  handleBlockChange(event) {
    this.setState({ block: event.target.value });
  }


  handleSubmit(event) {
    if(!this.state.email || this.state.email != this.state.email_confirm){
        alert('Emails are not matched!');
    }
    else if(!this.state.password || this.state.password != this.state.password_confirm){
        alert('Passwords are not matched!');
    }
    else if(this.state.password.length < 4){
        alert('Password should be 4 digits at least!');
    }
    else {
        $.post(appConfig.getServerUrl() + '/api/examusers', {
            'first_name': this.state.first_name,
            'last_name': this.state.last_name,
            'username': this.state.username,
            'email': this.state.email,
            'password': this.state.password,
            'block': this.state.block,
            'related_class': this.props.class_id,
            'related_programme': this.props.programme_id,
        }, (response) => {
            alert('Success submission.');
            this.context.router.push('/profil');
        }).fail(function (response) {
            let errors = "";
            Object.values(response.responseJSON).map((key) => {
                errors += key[0] + '\n';
            });

            alert(errors);
        });
    }
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} method={"POST"} className="form registrationForm">
        <section className="todo">
          <ul className="todo-list">
            <li className="formHead">Formulaire d'enregistrement</li>
            <li></li>
            <li>
              <label className="input-group">
                <p>Prénom :</p>
                <input
                  type="text"
                  className="form-control"
                  name={"first_name"}
                  value={this.state.first_name}
                  onChange={this.handleFnameChange}
                />
              </label>
            </li>
            <li>
              <label className="input-group">
                <p>Nom :</p>
                <input
                  type="text"
                  className="form-control"
                  name={"last_name"}
                  value={this.state.last_name}
                  onChange={this.handleLnameChange}
                />
              </label>
            </li>
            <li>
              <label className="input-group">
                <p>Login :</p>
                <input
                  type="text"
                  className="form-control"
                  name={"username"}
                  value={this.state.login}
                  onChange={this.handleUserNameChange}
                />
              </label>
            </li>
            <li>
              <label className="input-group">
                <p>Email :</p>
                <input
                  type="email"
                  className="form-control"
                  name={"email"}
                  value={this.state.email}
                  onChange={this.handleEmailChange}
                />
              </label>
            </li>
            <li>
              <label className="input-group">
                <p>Confirmation d'email :</p>
                <input
                  type="email"
                  className="form-control"
                  name={"email_confirm"}
                  value={this.state.email_confirm}
                  onChange={this.handleEmailConfirmChange}
                />
              </label>
            </li>
            <li>
              <label className="input-group">
                <p>Mot de passe :</p>
                <input
                  type="password"
                  className="form-control"
                  name={"password"}
                  value={this.state.password}
                  onChange={this.handlePasswordChange}
                  required={"required"}
                />
              </label>
            </li>
            <li>
              <label className="input-group">
                <p>Confirmation mot de passe :</p>
                <input
                  type="password"
                  className="form-control"
                  name={"confirm_password"}
                  onChange={this.handlePasswordConfirmChange}
                  required={"required"}
                />
              </label>
            </li>
            <li></li>
            <li>
              <input type="submit" className="form-control submitBtn" value="Je valide !" />
            </li>
          </ul>
          {/*
          <label className="input-group">
            <h3>Offer:</h3>
            <select
              className="form-control"
              name={"block_list"}
              onChange={this.handleBlockChange}
            >
                <option value="" selected={"selected"}>Whole programme</option>
                {
                    Object.keys(this.state.block_list).map((key) => {
                        return(
                            <option value={this.state.block_list[key].id} key={key}>{this.state.block_list[key].name}</option>
                        )
                    })
                }
            </select>
          </label>
          */}
          </section>
      </form>
    );
  }
}

RegistrationForm.contextTypes = {
    router: React.PropTypes.object.isRequired
};
export default RegistrationForm;
