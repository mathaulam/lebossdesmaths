from __future__ import unicode_literals

from django.db import models

class Classe(models.Model):
    nom         = models.CharField(max_length=255)
    ordre       = models.PositiveSmallIntegerField()

    class Meta(object):
        ordering = ('ordre',)

    def __unicode__(self):
        return u'%s' % self.nom
