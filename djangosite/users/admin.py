from django.contrib import admin
from users.models import Profile

class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'classe', 'pseudo_classe']
    search_fields = ['user__username', 'user__first_name', 'user__last_name']
    pass

admin.site.register(Profile, ProfileAdmin)