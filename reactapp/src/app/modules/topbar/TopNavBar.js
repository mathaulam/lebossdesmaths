import React from "react";
import {Link} from 'react-router';
import UserBox from "app/modules/topbar/UserBox";
import CoursList from "app/modules/topbar/CoursList";
import {auth} from 'app/tools/Auth';

class TopNavBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <nav className="navbar navbar-default navbar-top">
                <div className="container">

                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <Link className="navbar-brand" to="/">La Boss Des Maths</Link>
                    </div>

                    <div id="navbar" className="navbar-collapse collapse navbar-right">
                        <ul className="nav navbar-nav menu">
                            <li className="dropdown">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Cours <span className="caret"></span>
                                </a>
                                <CoursList changePage={this.props.changePage} />
                            </li>
                            {auth.loggedIn() && 
                            <li><Link to="/eleve-qcms">QCMs</Link></li>
                            }
                            <UserBox />
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

export default TopNavBar;