import React from "react";
import TopNavBar from "app/modules/topbar/TopNavBar";
import BasePage from "app/pages/BasePage";
import "app/pages/pages.css";

class Poc extends BasePage {
    
    constructor(props) {
        super(props);

        this.state = {question: null};
        this.step = 0;

        this.loaded = false;
        this.player = null;
        this.done = false;
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        this.loadYoutubeAPI = this.loadYoutubeAPI.bind(this);
        this.onPlayerReady = this.onPlayerReady.bind(this);
        this.onPlayerStateChange = this.onPlayerStateChange.bind(this);
        this.stopVideo = this.stopVideo.bind(this);

    }

    componentDidMount() {
        this.loadYoutubeAPI();
    }

    loadYoutubeAPI() {
        if (!this.loaded) {
            
            this.loaded = new Promise((resolve) => {
                const tag = document.createElement('script');
                tag.src = 'https://www.youtube.com/iframe_api';
                const firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                window.onYouTubeIframeAPIReady = () => resolve(window.YT);
            });
            
            this.loaded.then((YT) => {
                this.player = new YT.Player('player', {
                    height: this.props.height || 390,
                    width: this.props.width || 640,
                    // videoId: 'M7lc1UVf-VE',
                    playerVars: {
                        'controls': 1,
                        'modestbranding': 1,
                        'rel': 0,
                        'showinfo': 0,
                        'hl': 'fr',
                        'fs': 0,
                    },
                    events: {
                        'onReady': this.onPlayerReady,
                        'onStateChange': this.onPlayerStateChange
                    }
                })
            });
        }
    }

    onPlayerReady(event) {
        this.player.loadVideoById({'videoId': 'qrvIcYEhoAY',
               'startSeconds': 0,
               'suggestedQuality': 'large'});
    }

    onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.ENDED) {
          this.showQuestion();
        }
    }

    stopVideo() {
        this.player.stopVideo();
    }

    showQuestion() {
        if (this.step <= 1) {
            this.setState({question: 1});
        }
    }

    nextVideo() {
        this.step += 1;
        let videoId = null;
        if (this.step == 1) {
            videoId = 'yqo-5AtbCZE';
        } else if (this.step == 2) {
            videoId = 'KJBPcJw27IU';
        }
        this.player.loadVideoById({'videoId': videoId,
               'startSeconds': 0,
               'suggestedQuality': 'large'});
        this.setState({question: null});
    }

    render() {
            return (
                <div>
                    <TopNavBar />
                    <div className="container">
                        <div className="jumbotron">
                            <h2>Test Videos Youtube Dynamiques</h2>
                            <div id="player"></div>
                            {this.state.question &&
                                <div>
                                    <h3>Question:</h3>
                                    <div id="question">
                                    <button onClick={() => this.nextVideo()}>Suite</button>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            );
        }

}

export default Poc;