import React, { Component } from 'react';
import McqControls from './McqControls';
import McqSequence from './McqSequence';
import './Mcq.css';

class McqWizard extends Component {

    constructor(props) {
        super(props);
        this.state = {currentStep: 0, icons: {}};
        this.changeStep = this.changeStep.bind(this);
        this.setIconStatus = this.setIconStatus.bind(this);
    }

    changeStep(step) {
        this.setState({currentStep: step});
    }

    setIconStatus(step, status) {
        this.setState((prevState) => {
            prevState.icons[step] = status;
            return {state: prevState.icons};
        });
    }

    render() {
        return (
            <div>
                <div className="wizard">
                    <McqControls 
                        currentStep={this.state.currentStep}
                        changeStep={this.changeStep}
                        icons={this.state.icons}
                    />
                </div>
                <div className="mcq-sequence">
                    <McqSequence 
                        qcms={this.props.qcms}
                        seqId={this.props.seqId}
                        currentStep={this.state.currentStep}
                        changeStep={this.changeStep}
                        setIconStatus={this.setIconStatus}
                    />
                </div>
            </div>
        )
    }
}

export default McqWizard;
