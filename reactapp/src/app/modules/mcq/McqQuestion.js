import React, { Component } from 'react';
import {findAncestorByClass} from 'app/tools/Helpers.js'
import 'app/modules/mcq/Mcq.css';
import 'app/app.css';

class McqQuestion extends Component {

    constructor(props) {
        super(props);

        // Bind methods to this
        this.handleSelection = this.handleSelection.bind(this);
        this.submitSelection = this.submitSelection.bind(this);
    }

    handleSelection(e) {
        e.preventDefault();

        // Don't let the user select an answer if already submitted results
        if (this.props.finished) {
            return;
        }

        // Set chosen answer in state
        let choiceElem = findAncestorByClass(e.target, 'btn-choice');
        this.setState({selected: choiceElem.dataset.index});
        this.props.questionObj.chosen = choiceElem.dataset.index

        // Notify that an answer was chosen for the Control bar
        this.props.setIconStatus(this.props.currentStep, "answered");
    }

    submitSelection(e) {
        e.preventDefault();
        this.props.nextAction(this.props.questionObj.chosen);
    }

    render() {
        return (
            <div className="mcqContainer">
                <div className="questionContainer noselect" dangerouslySetInnerHTML={{__html: this.props.questionObj.question}}></div>
                <ul>
                {
                    Object.keys(this.props.questionObj.choices).map((key) => {
                        let classNames = "btn btn-default btn-choice noselect";

                        // Selected by user
                        if (this.props.questionObj.chosen == key) {
                            classNames += " choiceSelected";
                        }

                        // Showing true answer
                        if (this.props.finished && this.props.questionObj.answer == key) {
                            classNames += " correctAnswer";
                        }

                        return (
                            <li key={key}>
                                <a data-index={key} 
                                    onClick={this.handleSelection}
                                    className={classNames}
                                    dangerouslySetInnerHTML={{__html: this.props.questionObj.choices[key].text}}>
                                </a>
                            </li>
                        )
                    })
                }
                </ul>
                <div className="mcqControls">
                    <button type="button" onClick={this.submitSelection} className="btn next-step btn-info">Suivant</button>
                </div>
            </div>
        )
    }
}

export default McqQuestion;
