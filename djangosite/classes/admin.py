from django.contrib import admin
from adminsortable2.admin import SortableAdminMixin
from adminsortable2.admin import SortableInlineAdminMixin
from classes.models import Classe
from cours.models import Cours


class CoursInline(SortableInlineAdminMixin, admin.StackedInline):
    model = Cours
    exclude = ['video', 'contenu', 'bilan', 'methodes', 'exercices']


class ClasseAdmin(SortableAdminMixin, admin.ModelAdmin):
    inlines = (CoursInline,)


admin.site.register(Classe, ClasseAdmin)