from __future__ import unicode_literals

from itertools import chain

from django.db import models
from django.contrib.auth.models import User
from classes.models import Classe
from django.db.models.signals import post_save
from django.dispatch import receiver

from cours.models import Block


class Profile(models.Model):

    # Helpers.
    subscription_plans = (
        (1, 'Offre Complete'),
        (2, 'Only Bloc'),
    )

    # Attributes.
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    classe = models.ForeignKey(Classe, on_delete=models.SET_NULL, blank=True, null=True)
    pseudo_classe = models.CharField(max_length=255, blank=True)
    offer = models.ForeignKey(
        to='landing_pages.SublandingPage',
        related_name='users',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    programmes = models.ManyToManyField(
        to='cours.Programme',
        related_name='users',
        blank=True
    )
    blocks = models.ManyToManyField(
        to='cours.Block',
        related_name='users',
        blank=True,
    )

    def get_user_blocks(self):
        """
        Returns a list of all Blocks that relate to user.
        :return: BlocksQuerySet
        """

        blocks = Block.objects.filter(id=0)

        # Blocks that relate to certain programme.
        if self.programmes.all():
            blocks = self.programmes.first().blocks.all()
            return blocks

        # Blocks that user is subscribe to independently.
        if self.blocks.all():
            blocks = self.blocks.all()

        return blocks

    def get_current_programme(self):
        """
        Returns the user's first programme.
        """
        try:
            return self.programmes.first()
        except AttributeError:
            return





@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
