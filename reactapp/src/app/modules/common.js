import React from "react";

export function getSmallSpinner() {
    return(
        <img src="/static/img/spin.svg" className="center-block" />
    );
}