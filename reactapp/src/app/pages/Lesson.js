import React from "react";
import {Link} from 'react-router';
import {httpClient} from 'app/tools/HttpClient';
import BasePage from "./BasePage";
import TopNavBar from "app/modules/topbar/TopNavBar";
import {getErrorMsg, getRandomInt} from 'app/tools/Helpers.js';
import "app/pages/pages.css";

class Lesson extends BasePage {

    constructor(props) {
        super(props);
        this.state = {lesson:null, coursList:null};
        this.loadData = this.loadData.bind(this);
        this.loadData(this.props.params.id);

        this.illustrations = [
            '/img/illustrations/ballistique.png',
            '/img/illustrations/eleve_calco.png',
            '/img/illustrations/eleve_reflechit.png',
            '/img/illustrations/eleve_trace_cercle.png',
            '/img/illustrations/mesure_carre.png',
        ]
    }

    componentWillReceiveProps(nextProps) {
        this.setState({lesson:null, coursList:null});
        this.loadData(nextProps.params.id);
    }

    loadData(LessonId) {

        // Get name of classe
        httpClient.get(
            "/api/lessons/" + LessonId,
            (resp) => {
                this.setState({lesson: resp.data});
                this.loadCours(LessonId);
            },
            (error) => alert(getErrorMsg(error))
        );
    }

    loadCours(LessonId) {
        // Get list of cours
        httpClient.get(
            "/api/lessons/" + LessonId + "/grains",
            (resp) => this.setState({coursList: resp.data}),
            (error) => alert(getErrorMsg(error))
        );
    }

    getRandomIllustration() {
        let index = getRandomInt(0, this.illustrations.length);
        let url = this.illustrations[index];
        return <img src={url} alt="Illustration" className="img-responsive center-block"/>;
    }

    renderBody() {
        
        if (!this.state.coursList) {
            return this.showLoader();
        }

        return (
            <div>
                <section className="title-section">
                    <div className="container">
                        <div className="row">
                            <h1>{this.state.lesson.name}</h1>
                        </div>
                    </div>
                </section>
                <div className="container cours-list">
                    <div className="row">
                        <div className="col-xs-12 col-sm-8 col-lg-6 vcenter">
                            <section className="todo">
                                <ul className="todo-list">
                                    <li>Liste des grains de {this.state.lesson.name} :</li>
                                {
                                    this.state.coursList.map((object, i) => {
                                        return (
                                            <li key={object.id}>
                                                - <Link to={ '/cours/' + object.id}>{object.name}</Link>
                                            </li>
                                        )
                                    })
                                }
                                </ul>
                            </section>
                        </div>
                        <div className="col-xs-12 col-sm-4 col-lg-6 vcenter text-center">
                            {this.getRandomIllustration()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Lesson;