from __future__ import unicode_literals

from django.apps import AppConfig


class QcmsConfig(AppConfig):
    name = 'qcms'
    verbose_name = 'Exercises'
