from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from landing_pages.models import SublandingPage
from cours.models import Block, Programme
from users.models import Profile


class UserModelTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='test_user', password='05sd0s5ds5d0')
        self.user = User.objects.create_user(username='test_user2', password='05sd0s5ds5d0')
        self.profile = self.user.profile
        self.profile2 = self.user.profile
        self.block1 = Block.objects.create(name='block1')
        self.block2 = Block.objects.create(name='block2')
        self.block3 = Block.objects.create(name='block3')
        self.programme1 = Programme.objects.create(name='programme1')
        self.programme2 = Programme.objects.create(name='programme2')

    def test_user_blocks_with_user_in_programme(self):
        """Assert that function will return all blocks contained in programmes."""
        # Test setup
        self.programme1.blocks.add(self.block1, self.block2)
        self.programme2.blocks.add(self.block3)

        # Test body
        self.programme1.users.add(self.profile)
        self.programme2.users.add(self.profile)

        # Test assertion
        self.assertEqual(len(self.profile.get_user_blocks()), 2)
    
    def test_user_blocks_with_user_with_independent_blocks(self):
        """Assert that function will return blocks that user relate to independently."""
        # Test setup
        # Test body
        self.block1.users.add(self.profile2)
        self.block3.users.add(self.profile2)

        # Test assertion
        self.assertEqual(len(self.profile.get_user_blocks()), 2)

    def test_user_blocks_with_none(self):
        """Assert that function will return empty list if no programmes or blocks."""
        # Test setup
        # Test body
        # Test assertion
        self.assertEqual(len(self.profile.get_user_blocks()), 0)

    def test_user_blocks_with_both(self):
        """Assert that function will return only the programmes blocks in case of both."""
        # Test setup
        self.programme1.blocks.add(self.block1)
        self.programme2.blocks.add(self.block3)

        # Test body
        self.programme1.users.add(self.profile)
        self.programme2.users.add(self.profile)
        self.block2.users.add(self.profile)

        # Test assertion
        self.assertEqual(len(self.profile.get_user_blocks()), 1)


class ExamUserViewsTest(TestCase):

    def test_create_new_user(self):
        """Assert that new user will be created successfully."""
        # Test setup
        data = {
            'username': 'test_user',
            'email': 'test@test.com',
            'password': '123465798testtest'
        }

        # Test body
        request = self.client.post('/api/examusers', data=data)
        last_user = User.objects.last()

        # Test assertion
        print(request)
        self.assertEqual(request.status_code, 201)
        self.assertEqual(last_user.username, 'test_user')

    def test_new_user_profile(self):
        """Assert that profile is populated successfully after registration."""
        # Test setup
        exam = SublandingPage.objects.create(name='test_exam')
        data = {
            'username': 'test_user',
            'email': 'test@test.com',
            'password': '123465798testtest',
        }

        # Test body
        request = self.client.post('/api/examusers', data=data)
        last_user = User.objects.last()
        last_profile = Profile.objects.last()

        # Test assertion
        print(request)
        self.assertEqual(request.status_code, 201)
        self.assertEqual(last_user.username, 'test_user')
        self.assertEqual(last_user, last_profile.user)

    def test_new_user_profile_without_plan_and_exam(self):
        """Assert that profile is populated successfully after registration if no plan or exam."""
        # Test setup
        exam = SublandingPage.objects.create(name='test_exam')
        data = {
            'username': 'test_user',
            'email': 'test@test.com',
            'password': '123465798testtest',
        }

        # Test body
        request = self.client.post('/api/examusers', data=data)
        last_user = User.objects.last()
        last_profile = Profile.objects.last()

        # Test assertion
        print(request)
        self.assertEqual(request.status_code, 201)
        self.assertEqual(last_user.username, 'test_user')
        self.assertEqual(last_user, last_profile.user)

