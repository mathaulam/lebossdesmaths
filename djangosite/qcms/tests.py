#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase, APIRequestFactory, force_authenticate

from cours.models import Grain
from qcms.models import ExerciseTypes, GeneralExercise
from qcms.views import QcmAdminImportView


class TestMassImportView(APITestCase):
    """
    A test suite for the mass import view.
    """

    def setUp(self):
        self.grain = Grain.objects.create(
            name='test_grain',
        )
        self.exercise_type = ExerciseTypes.objects.create(
            type='MCQ_test'
        )
        self.admin = User.objects.create_superuser(
            username='admin_test',
            email='asds@sdasd.com',
            password='2sdas9d84sdx5cx41w5ds'
        )
        self.factory = APIRequestFactory()
        self.view = QcmAdminImportView.as_view()

    def test_mass_import_mcq_with_multiple_exercises(self):
        """Assert that mass import view will create exercises based on body."""
        # Test setup
        body = """
                ddddd
                type:"""+str(self.exercise_type.id)+"""
                diff: 1
                que: On considère la fonction $f$  dont la courbe est représentée ci-dessous :
                <pre>
                [latex]
                \begin{pspicture}(-1,-1)(1,1)
                \psgrid[griddots=10,gridlabels=0pt, subgriddiv=0, gridcolor=black!20]
                \psaxes(0,0)(-2,-2)(2,2)
                \psplot[linecolor=red]{-2}{2}{-x}
                \end{pspicture}
                [/latex]
                </pre>
                Que vaut l'image de $0$ par la fonction $f$?
                ss1: $5$
                ss2: $-5$
                ss3: $\frac{1}{2}$
                ss4: $-1$
                fffff
                ddddd
                type:"""+str(self.exercise_type.id)+"""
                diff: 2
                pre: 
                eno: On considère la fonction $f$  dont la courbe est représentée ci-dessous :
                <center> <img src="static/fgrgr1-1.png" alt="Courbe" height="300" width="300">  </center> 
                que: Que vaut l'image de $2$ par la fonction $f$?
                ss1: Il n'y en a pas
                ss2: $1,5$
                ss3: $\frac{3}{2}$
                ss4: $3$
                fffff
                """
        data = {
            "grain": self.grain.id,
            "data": body
        }
        url = '/api/admin-qcm-import/'
        # Test body
        request = self.factory.post(path=url, data=json.dumps(data), content_type='json')
        token = Token.objects.get(user=self.admin)
        request.META['HTTP_AUTHORIZATION'] = 'Token {0}'.format(token)
        force_authenticate(request, self.admin, token)
        response = self.view(request)
        # Test assertion
        print(response)
        execerses_count = GeneralExercise.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertGreater(execerses_count, 0)

    def test_mass_import_mcq_with_different_exercises(self):
        """Assert that mass import view will create exercises based on body."""
        # Test setup
        another_type = ExerciseTypes.objects.create(
            type='holes'
        )
        body = """
                ddddd 
                type:"""+str(self.exercise_type.id)+"""
                diff: 1
                que: On considère la fonction $f$  dont la courbe est représentée ci-dessous :
                <pre>
                [latex]
                \begin{pspicture}(-1,-1)(1,1)
                \psgrid[griddots=10,gridlabels=0pt, subgriddiv=0, gridcolor=black!20]
                \psaxes(0,0)(-2,-2)(2,2)
                \psplot[linecolor=red]{-2}{2}{-x}
                \end{pspicture}
                [/latex]
                </pre>
                Que vaut l'image de $0$ par la fonction $f$?
                ss1: $5$
                ss2: $-5$
                ss3: $\frac{1}{2}$
                ss4: $-1$
                fffff
                ddddd
                type:"""+str(another_type.id)+"""
                diff: 3
                pre: 
                eno: On considère la fonction $f$  dont la courbe est représentée ci-dessous :
                <center> <img src="static/fgrgr1-1.png" alt="Courbe" height="300" width="300">  </center> 
                que: Que vaut l'image de $2$ par la fonction $f$?
                ss1: Il n'y en a pas
                fffff
                """
        data = {
            "grain": self.grain.id,
            "data": body
        }
        url = '/api/admin-qcm-import/'
        # Test body
        request = self.factory.post(path=url, data=json.dumps(data), content_type='json')
        token = Token.objects.get(user=self.admin)
        request.META['HTTP_AUTHORIZATION'] = 'Token {0}'.format(token)
        force_authenticate(request, self.admin, token)
        response = self.view(request)
        # Test assertion
        print(response)
        execerses_count = GeneralExercise.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertGreater(execerses_count, 0)

    def test_mass_import_with_different_start_and_end(self):
        """Assert that mass import view will create exercises if there're different number of fffffs."""
        # Test setup
        another_type = ExerciseTypes.objects.create(
            type='holes'
        )
        body = """
                ddddd 
                type:"""+str(self.exercise_type.id)+"""
                diff: 1
                que: On considère la fonction $f$  dont la courbe est représentée ci-dessous :
                <pre>
                [latex]
                \begin{pspicture}(-1,-1)(1,1)
                \psgrid[griddots=10,gridlabels=0pt, subgriddiv=0, gridcolor=black!20]
                \psaxes(0,0)(-2,-2)(2,2)
                \psplot[linecolor=red]{-2}{2}{-x}
                \end{pspicture}
                [/latex]
                </pre>
                Que vaut l'image de $0$ par la fonction $f$?
                ss1: $5$
                ss2: $-5$
                ss3: $\frac{1}{2}$
                ss4: $-1$
                fffff
                ddddd 
                num:10-1002
                type:"""+str(another_type.id)+"""
                diff: 1
                pre: 
                niv: 0
                eno: On considère la fonction $f$  dont la courbe est représentée ci-dessous :
                <center> <img src="static/fgrgr1-1.png" alt="Courbe" height="300" width="300">  </center> 
                que: Que vaut l'image de $2$ par la fonction $f$?
                ss1: Il n'y en a pas
                fffff
                """
        data = {
            "grain": self.grain.id,
            "data": body
        }
        url = '/api/admin-qcm-import/'
        # Test body
        request = self.factory.post(path=url, data=json.dumps(data), content_type='json')
        token = Token.objects.get(user=self.admin)
        request.META['HTTP_AUTHORIZATION'] = 'Token {0}'.format(token)
        force_authenticate(request, self.admin, token)
        response = self.view(request)
        # Test assertion
        print(response)
        execerses_count = GeneralExercise.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertGreater(execerses_count, 0)

    def test_ratio_with_float_number(self):
        """Assert that mass import will accept float number of ratio."""
        # Test setup
        another_type = ExerciseTypes.objects.create(
            type='holes'
        )
        body = """
                ddddd 
                type:""" + str(self.exercise_type.id) + """
                diff: 1
                ratio: 0.75
                que: On considère la fonction $f$  dont la courbe est représentée ci-dessous :
                <pre>
                [latex]
                \begin{pspicture}(-1,-1)(1,1)
                \psgrid[griddots=10,gridlabels=0pt, subgriddiv=0, gridcolor=black!20]
                \psaxes(0,0)(-2,-2)(2,2)
                \psplot[linecolor=red]{-2}{2}{-x}
                \end{pspicture}
                [/latex]
                </pre>
                Que vaut l'image de $0$ par la fonction $f$?
                ss1: $5$
                ss2: $-5$
                ss3: $\frac{1}{2}$
                ss4: $-1$
                fffff
                """
        data = {
            "grain": self.grain.id,
            "data": body
        }
        url = '/api/admin-qcm-import/'
        # Test body
        request = self.factory.post(path=url, data=json.dumps(data), content_type='json')
        token = Token.objects.get(user=self.admin)
        request.META['HTTP_AUTHORIZATION'] = 'Token {0}'.format(token)
        force_authenticate(request, self.admin, token)
        response = self.view(request)
        # Test assertion
        print(response)
        execerses_count = GeneralExercise.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertGreater(execerses_count, 0)