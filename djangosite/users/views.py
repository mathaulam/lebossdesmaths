# -*- coding: utf-8 -*-
from django.contrib.auth import login
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from django.views import View
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.db import transaction
from django.core.validators import validate_email
from rest_framework import viewsets, status
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from common.helpers import get_token_user
from landing_pages.models import SublandingPage
from users.models import Profile
from users.serializers import UserSerializer, ExamUserSerializer
import json
import logging

log = logging.getLogger('django')


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, pk=None):
        # If superuser, allow to get another user's data
        if request.user.is_superuser and pk != 'me':
            user = User.objects.get(id=int(pk))
            return Response(UserSerializer(user, context={'request': request}).data)

        # If normal user, only allow to fetch its own data
        if pk == str(request.user.id) or pk == 'me':
            return Response(UserSerializer(request.user, context={'request': request}).data)

        raise PermissionDenied()

    def list(self, request):
        # If superuser, give him any user he wants
        if request.user.is_superuser:
            return super(UserViewSet, self).list(request)
        raise PermissionDenied()


class ExamUserView(viewsets.ModelViewSet):

    serializer_class = ExamUserSerializer
    model = User
    queryset = User.objects.all()
    permission_classes = (AllowAny,)

    def perform_create(self, serializer):
        """
        Add additional hashing for the sent password.
        """
        instance = serializer.save()
        instance.set_password(instance.password)
        instance.save()

    def create(self, request, *args, **kwargs):
        """
        Add extra information for user profile.
        """
        plan = request.POST.get('plan', None)
        block = request.POST.get('block', None)
        related_class = request.POST.get('related_class', None)
        related_programme = request.POST.get('related_programme', None)
        user_data = super(ExamUserView, self).create(request, *args, **kwargs)
        try:
            user = User.objects.get(username=user_data.data['username'])
            if plan:
                user.profile.programmes.add(plan)
            if block:
                user.profile.blocks.add(block)
            if related_class:
                user.profile.classe_id = related_class
            if related_programme:
                user.profile.programmes.add(related_programme)
            user.profile.save()
        except User.DoesNotExist:
            return Response(status=400)
        except AttributeError:
            Profile.objects.create(user=user)

        return Response(user_data.data, status=status.HTTP_201_CREATED)


@method_decorator(csrf_exempt, name='dispatch')
class UserProfileView(View):

    def post(self, request, *args, **kwargs):

        # Check admin authorization
        user = get_token_user(request)
        if not user:
            return HttpResponseForbidden("Droits insuffisants")

        try:
            with transaction.atomic():

                # Read the body text from request
                json_body = json.loads(request.body.decode('utf-8'))
                user.first_name = json_body.get('first_name', user.first_name)
                user.last_name = json_body.get('last_name', user.last_name)
                user.email = json_body.get('email', user.email)

                validate_email(user.email)
                user.save()

        except Exception, error:
            response = "Erreur lors de la mise a jour des donnees utilisateur: %s" % str(error)
            log.error(response)
            return HttpResponseBadRequest(response)

        return HttpResponse("Mise a jour reussie.")
