import React from "react";
import {httpClient} from 'app/tools/HttpClient';
import BasePage from "./BasePage";
import TopNavBar from "app/modules/topbar/TopNavBar";
import {getSmallSpinner} from 'app/modules/common.js';
import {getErrorMsg} from 'app/tools/Helpers.js';
import {DateField, Calendar, DatePicker} from 'react-date-picker';
import 'app/tools/react-date-picker.css';
import "app/pages/pages.css";

class AdminQcmGenerate extends BasePage {

    constructor(props) {
        super(props);
        this.state = {
            themeValue: '-1',
            expirationValue: this.getDefaultExpirationDate(),
            sending: false,
            themeList: null
        };
        this.handleThemeChange = this.handleThemeChange.bind(this);
        this.handleGenerate = this.handleGenerate.bind(this);
        this.handleSendEmail = this.handleSendEmail.bind(this);
        this.onDateChange = this.onDateChange.bind(this);
        this.loadThemeList();
    }

    loadThemeList() {
        // Get list of theme
        httpClient.get("/api/themes",
            (resp) => this.setState({themeList: resp.data}),
            (error) => alert(getErrorMsg(error))
        );
    }

    handleThemeChange(event) {
        this.setState({themeValue: event.target.value});
    }

    handleGenerate(event) {
        event.preventDefault();
        this.setState({sending: true});

        // Send QCM data
        httpClient.post(
            "/api/admin-qcm-generate/", 
            {theme: this.state.themeValue, expiration: this.state.expirationValue},
            (resp) => this.setState({sending: false, responseMsg: resp.data, responseSuccess: true}),
            (error) => {
                let errorMsg = getErrorMsg(error);
                this.setState({sending: false, responseMsg: errorMsg.substring(0, 500), responseSuccess: false});
            }
        );
    }

    handleSendEmail(event) {
        event.preventDefault();
        this.setState({sending: true});

        // Send QCM data
        httpClient.get(
            "/api/admin-qcm-generate?send_all_mails=true",
            (resp) => this.setState({sending: false, responseMsg: resp.data, responseSuccess: true}),
            (error) => {
                let errorMsg = getErrorMsg(error);
                this.setState({sending: false, responseMsg: errorMsg.substring(0, 500), responseSuccess: false});
            }
        );
    }

    onDateChange(dateString, { dateMoment, timestamp }) {
        this.setState({expirationValue: dateString});
    }

    getDefaultExpirationDate() {
        let expiration = new Date();
        expiration.setDate(expiration.getDate() + 3);
        expiration.setUTCHours(23);
        expiration.setUTCMinutes(59);
        expiration.setUTCSeconds(59);
        return expiration.toISOString().substr(0, 19).replace('T', ' ');
    }

    renderBody() {

        return (
            <div>
                <section className="title-section">
                    <div className="container">
                        <div className="row">
                            <h1>Génération d'un QCM</h1>
                        </div>
                    </div>
                </section>
                <div className="container">
                    <div className="jumbotron">
                        <form>
                            <div className="form-group">
                                <label htmlFor="theme">Thème:</label>
                                <select id="theme" value={this.state.themeValue} onChange={this.handleThemeChange} className="form-control">
                                    <option value="-1">-----</option>
                                    { this.state.themeList && 
                                        this.state.themeList.map((object, i) => {
                                            return (
                                                <option key={object.id} value={object.id}>{object.fullname}</option>
                                            );
                                        })
                                    }
                                </select>
                            </div>
                            <div className="form-group">
                                <div><b>Date d'expiration:</b></div>
                                <DateField
                                    dateFormat="YYYY-MM-DD HH:mm:ss"
                                    forceValidDate={true}
                                    expandOnFocus={false}
                                    updateOnDateClick={true}
                                    defaultValue={this.getDefaultExpirationDate()}
                                    onChange={this.onDateChange}
                                >
                                    <DatePicker
                                        navigation={true}
                                        locale="fr"
                                        forceValidDate={true}
                                        highlightWeekends={true}
                                        highlightToday={true}
                                        weekNumbers={false}
                                        weekStartDay={1}
                                        footer={false}
                                    />
                                </DateField>
                            </div>    
                            {this.state.responseMsg && this.state.responseSuccess &&
                                <div className="alert alert-info">{this.state.responseMsg}</div>
                            }
                            {this.state.responseMsg && !this.state.responseSuccess &&
                                <div className="alert alert-danger">{this.state.responseMsg}</div>
                            }
                            <div className="form-group">
                                {this.state.sending ?
                                    getSmallSpinner()
                                :
                                    (
                                        <div className="text-center">
                                            <input type="submit" value="Générer"  onClick={this.handleGenerate} className="btn button-animated"/>
                                            &nbsp; &nbsp;
                                            <input type="submit" value="Envoyer Mails"  onClick={this.handleSendEmail} className="btn button-animated"/>
                                        </div>
                                    )
                                }
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default AdminQcmGenerate;