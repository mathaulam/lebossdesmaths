import React from "react";
import {Link} from 'react-router';
import ReactPlayer from 'react-player'
import {httpClient} from 'app/tools/HttpClient';
import BasePage from "./BasePage";
import TopNavBar from "app/modules/topbar/TopNavBar";
import {preprocessHtml, getErrorMsg} from 'app/tools/Helpers.js';
import "app/pages/pages.css";

class Cours extends BasePage {

    constructor(props) {
        super(props);
        this.state = {cours:null};
        this.loadData = this.loadData.bind(this);
        this.loadData();
    }

    loadData() {
        let coursId = this.props.params.id;

        // Get name of classe
        httpClient.get(
            "/api/cours/" + coursId,
            (resp) => {
                this.setState({cours: resp.data});
            },
            (error) => alert(getErrorMsg(error))
        );
    }

    componentDidUpdate() {
        // Re-init Mathjax after html has been rendered
        MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
        $('body').latex();
    }

    renderBody() {
        
        if (!this.state.cours) {
            return this.showLoader();
        }

        return (
            <div>
                <section className="title-section">
                    <div className="container">
                        <div className="row">
                            <h1>{this.state.cours.nom}</h1>
                        </div>
                    </div>
                </section>
                <div className="container">
                    <div className="row">
                        {this.state.cours.video && this.state.cours.video.length > 0 &&
                        <ReactPlayer url={this.state.cours.video} controls={true} width="100%" className="videoContainer" />
                        }

                        <div className="cours-contents">
                            {this.state.cours.processed.contenu && this.state.cours.processed.contenu.trim().length > 0 &&
                                <div>
                                    <h1>Cours</h1>
                                    <div dangerouslySetInnerHTML={{__html: preprocessHtml(this.state.cours.processed.contenu)}} ></div>
                                </div>
                            }
                            {this.state.cours.processed.bilan && this.state.cours.processed.bilan.trim().length > 0 &&
                                <div>
                                    <h1>Bilan</h1>
                                    <div dangerouslySetInnerHTML={{__html: preprocessHtml(this.state.cours.processed.bilan)}} ></div>
                                </div>
                            }
                            {this.state.cours.processed.methodes && this.state.cours.processed.methodes.trim().length > 0 &&
                                <div>
                                    <h1>Méthodes</h1>
                                    <div dangerouslySetInnerHTML={{__html: preprocessHtml(this.state.cours.processed.methodes)}} ></div>
                                </div>
                            }
                            {this.state.cours.processed.exercices && this.state.cours.processed.exercices.trim().length > 0 &&
                                <div>
                                    <h1>Exercices</h1>
                                    <div dangerouslySetInnerHTML={{__html: preprocessHtml(this.state.cours.processed.exercices)}} ></div>
                                </div>
                            }
                            {this.state.cours.linked_epreuves && this.state.cours.linked_epreuves.length > 0 &&
                                <div>
                                    <h1>Epreuve à trous</h1>
                                    <p>Lorsque vous avez bien compris le cours, essayez-vous à l'épreuve à trous !</p>
                                    {this.state.cours.linked_epreuves.length == 1 &&
                                        <Link to={ '/epreuves/' + this.state.cours.linked_epreuves[0]}>
                                            <b>Aller à l'épreuve</b>
                                        </Link>
                                    }
                                    {this.state.cours.linked_epreuves.length > 1 &&
                                        <ul>
                                        {
                                            this.state.cours.linked_epreuves.map((id, i) => {
                                                return (
                                                    <li key={id}>
                                                        <Link to={ '/epreuves/' + id}>
                                                            <b>Epreuve {i+1}</b>
                                                        </Link>
                                                    </li>
                                                )
                                            })
                                        }
                                        </ul>
                                    }
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Cours;