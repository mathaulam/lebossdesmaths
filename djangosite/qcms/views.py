# -*- coding: utf-8 -*-
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest, \
 HttpResponseForbidden, HttpResponseServerError
from django.views import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist, FieldDoesNotExist
from django.core.mail import send_mail
from django.db import transaction
from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from common.helpers import check_admin_token, get_token_user
from qcms.helpers import get_participation_rate, get_medaille_level, upgrade_medaille
from qcms.models import Qcm, QcmSequence, Medaille, GeneralExercise, ExerciseTypes
from qcms.serializers import QcmSerializer, QcmSequenceSerializer
from cours.models import Theme
from users.models import Profile
from datetime import datetime
import json
import re
import random
import logging

log = logging.getLogger('django')

QUESTIONS_PER_QCM = 5
MAIL_TEMPLATE = u'''Bonjour %s,

Il y a un nouveau QCM: %s.
Il faut le faire avant le: %s.

À bientot !

La Boss des Maths
'''


class QcmViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows qcms to be viewed.
    Only admins can view these resources
    (otherwise students would be able to steal answers!)
    """
    queryset = Qcm.objects.all()
    serializer_class = QcmSerializer
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAdminUser,)


def generate_random_qcm_list(theme, niveau=5):
    """
    Picks random series from a dictionary of series. For each
    serie, it picks a single variante. Returns a json string containing
    the QCM IDs as keys associated to null values. Example:
    {"2": null, "6": null}
    """

    # Fetch all suitable series for the given theme
    theme_qcms = Qcm.objects.filter(theme=theme, niveau__lte=niveau)
    series = {}
    for qcm in theme_qcms:
        if qcm.serie not in series.keys():
            series[qcm.serie] = list()
        series[qcm.serie].append(qcm)

    # Get the number of series to take for the final list
    if len(series.keys()) < QUESTIONS_PER_QCM:
        msg = "Nombre de séries insuffisant dans ce thème. Il en faut au moins %s." % QUESTIONS_PER_QCM
        raise Exception(msg)

    # Take a sample of the available series for the final list
    qcm_list = {}
    series_choices = random.sample(series.keys(), QUESTIONS_PER_QCM)
    for serie in series_choices:
        variantes = series[serie]
        chosen_variante = random.choice(variantes)
        qcm_list[chosen_variante.id] = None
    return qcm_list


@method_decorator(csrf_exempt, name='dispatch')
class QcmSequenceView(View):

    def __get_existing_sequence_qcms(self, seq_id, user):
        """
        Returns all QCMs from a previously generated QCM Sequence
        (created by the teacher) for an exam run.
        """
        qcm_seq = QcmSequence.objects.get(pk=seq_id)
        if qcm_seq.eleve.id != user.id:
            return HttpResponseForbidden("Le QCM ne correspond pas a l'eleve.")
        if qcm_seq.expiration < datetime.now():
            return HttpResponseForbidden("Le QCM a passé sa date d'expiration.")
        if qcm_seq.score is not None:
            return HttpResponseForbidden("Le QCM a déjà été traité.")
        qcm_ids = [int(pk) for pk in json.loads(qcm_seq.qcm_list).keys()]
        qcms = Qcm.objects.filter(id__in=qcm_ids)
        serializer = QcmSerializer(qcms, many=True)
        return JsonResponse(serializer.data, safe=False)

    def __get_sequences_for_user(self, user_id, user):
        """
        Returns the list of all QcmSequence objects to display in the Student QCM page
        on the front-end.
        """
        if user_id != user.id:
            return HttpResponseForbidden("L'identifiant ne correspond pas a l'eleve.")
        seqs = QcmSequence.objects.filter(eleve=user_id).order_by('-expiration')
        serializer = QcmSequenceSerializer(seqs, many=True)
        return JsonResponse(serializer.data, safe=False)

    def __get_training_sequence_qcms(self, theme_id, user):
        """
        Returns a randomly generated list of QCMs for practice.
        """
        theme = Theme.objects.get(pk=theme_id)

        # Get the student's level
        current_medaille = get_medaille_level(user.id, theme_id)
        niveau = 0 if not current_medaille else current_medaille

        # Generate the practice QCM
        random_qcms = generate_random_qcm_list(theme, niveau=niveau)
        qcm_ids = [int(pk) for pk in random_qcms.keys()]
        qcms = Qcm.objects.filter(id__in=qcm_ids)
        serializer = QcmSerializer(qcms, many=True)
        return JsonResponse(serializer.data, safe=False)

    def get(self, request, *args, **kwargs):

        # Check authorization
        user = get_token_user(request)
        if not user:
            return HttpResponseForbidden("Droits insuffisants")

        try:
            seq_id = int(request.GET.get('seqId', 0))
            user_id = int(request.GET.get('userId', 0))
            theme_id = int(request.GET.get('themeId', 0))

            if seq_id > 0:
                return self.__get_existing_sequence_qcms(seq_id, user)
            elif user_id > 0:
                return self.__get_sequences_for_user(user_id, user)
            elif theme_id > 0:
                return self.__get_training_sequence_qcms(theme_id, user)
            else:
                return HttpResponseBadRequest()

        except Exception, error:
            response = "Erreur: %s" % str(error)
            log.error(response)
            return HttpResponseBadRequest(response)

    def post(self, request, *args, **kwargs):

        # Check authorization
        user = get_token_user(request)
        if not user:
            return HttpResponseForbidden("Droits insuffisants")

        try:
            seq_id = request.GET.get('seqId', 0)
            qcm_seq = QcmSequence.objects.get(pk=seq_id)
            if qcm_seq.eleve.id != user.id:
                return HttpResponseForbidden("Le QCM ne correspond pas a l'eleve.")

            # Read the body text from request and save as result
            json_body = json.loads(request.body.decode('utf-8'))
            score = 0
            for key, val in json_body.iteritems():
                if val == 4:
                    score += 1
            qcm_seq.qcm_list = json.dumps(json_body)
            qcm_seq.score = score
            qcm_seq.save()

            # Upgrade to next medal if the score allows it
            message = ""
            if qcm_seq.theme:
                message = upgrade_medaille(user.id, qcm_seq.theme.id, score)

        except Exception, error:
            response = "Erreur lors de la sauvegarde des resultats: %s" % str(error)
            log.error(response)
            return HttpResponseBadRequest(response)

        return HttpResponse(message)


@method_decorator(csrf_exempt, name='dispatch')
class QcmAdminImportView(View):

    DELIMITER_START = 'ddddd'
    DELIMITER_STOP = 'fffff'

    def __parse_single_value_or_fail(self, fieldname, regex, text):
        matches = re.findall(regex, text, re.S)
        if len(matches) == 1 and len(matches[0].strip()) > 0:
            return matches[0].strip()
        raise FieldDoesNotExist("Erreur lors de la lecture du champ '%s'" % fieldname)

    def __parse_single_optional_value(self, fieldname, regex, text):
        matches = re.findall(regex, text, re.S)
        if len(matches) == 1 and len(matches[0].strip()) > 0:
            return matches[0].strip()
        return None

    def post(self, request, *args, **kwargs):

        # Check admin authorization
        if not check_admin_token(request):
            return HttpResponseForbidden("Droits insuffisants")

        # Keep track of the qcm being processed for error handling
        count = 0
        num = None

        try:
            # Read the body text from request
            json_body = json.loads(request.body.decode('utf-8'))
            grain_id = int(json_body['grain'])
            import_data = json_body['data']

            # Create an import date for the whole batch
            import_date = datetime.now()

            # Only commit all new QCMs if the whole batch process went well
            with transaction.atomic():

                # Loop through each delimited QCM
                for qcm_data in re.findall('(' + self.DELIMITER_START + '.*?' + self.DELIMITER_STOP + ')', import_data, re.S):
                    count += 1
                    num = None

                    # Read all fields
                    type = self.__parse_single_value_or_fail('type', 'type:(.*?)\n', qcm_data)
                    diff = self.__parse_single_value_or_fail('diff', 'diff:(.*?)\n', qcm_data)
                    que = self.__parse_single_value_or_fail('que', 'que:(.*?)ss1:', qcm_data)
                    ratio = self.__parse_single_optional_value('ratio', 'ratio:(.*?)\n', qcm_data)
                    ss1 = self.__parse_single_optional_value('ss1', 'ss1:(.*?)ss2', qcm_data)
                    ss2 = self.__parse_single_optional_value('ss2', 'ss2:(.*?)ss3', qcm_data)
                    ss3 = self.__parse_single_optional_value('ss3', 'ss3:(.*?)ss4', qcm_data)
                    ss4 = self.__parse_single_optional_value('ss4', 'ss4:(.*?)' + self.DELIMITER_STOP, qcm_data)

                    # Determine the exercise type
                    exercise_type = ExerciseTypes.objects.get(id=type)
                    GeneralExercise.objects.create(
                        type_id=exercise_type.id,
                        grain_id=grain_id,
                        ratio=ratio or 1,
                        difficulty=diff,
                        variante=random.randint(len(que) - 1, len(que) - 1),
                        date_import=import_date,
                        question=que,
                        reponse1=ss1,
                        reponse2=ss2,
                        reponse3=ss3,
                        reponse4=ss4
                    )

                    # # Save to database
                    # instance = Qcm()
                    # instance.theme_id = theme_id
                    # instance.date_import = import_date
                    # instance.serie = serie
                    # instance.variante = variante
                    # instance.niveau = niv
                    # instance.question = que
                    # instance.reponse1 = ss1
                    # instance.reponse2 = ss2
                    # instance.reponse3 = ss3
                    # instance.reponse4 = ss4
                    # instance.save()

                if count < 1:
                    raise ObjectDoesNotExist("Aucun QCM n'a ete trouve")

                return HttpResponse("Traitement Reussi. Nombre de QCMs créés: %d" % count)

        except Exception, error:
            qcm_identity = num if num else 'numero %d' % count
            error_msg = str(error)
            if 'Duplicate entry' in error_msg:
                error_msg = 'Un QCM ayant le meme ensemble (theme, serie, variante) existe deja dans la BDD'
            response = "Erreur lors du traitement du QCM %s : %s. Nombre de QCMs crees: 0." % (qcm_identity, error_msg)
            log.error(response)
            return HttpResponseBadRequest(response)


@method_decorator(csrf_exempt, name='dispatch')
class QcmAdminGenerateView(View):

    def __get_unsent_qcm_queryset(self):
        """
        Returns all QcmSequences which had not been sent by mail,
        not been completed by the student, and are not expired yet.
        """
        unsent_qcmseqs = QcmSequence.objects.filter(mail_envoye=False)
        unsent_qcmseqs = unsent_qcmseqs.filter(score__isnull=True)
        unsent_qcmseqs = unsent_qcmseqs.filter(expiration__gt=datetime.utcnow())
        return unsent_qcmseqs

    def get(self, request, *args, **kwargs):
        """
        This GET method is used for sending e-mails to all students
        to notify them about new or unsent QCM Sequences.
        """
        if not request.GET.get('send_all_mails', False):
            return HttpResponseBadRequest("Commande non reconnue")

        unsent_qcmseqs = self.__get_unsent_qcm_queryset()
        count = 0
        failed = []
        for qcmseq in unsent_qcmseqs:
            first_name = qcmseq.eleve.first_name
            eleve_email = qcmseq.eleve.email
            if not eleve_email:
                continue
            subject = u'Nouveau QCM sur LaBossDesMaths'
            link = 'http://www.labossdesmaths.fr/app/qcm?seqId=' + str(qcmseq.id)
            body = MAIL_TEMPLATE % (first_name, link, qcmseq.expiration)
            from_email = 'qcm@labossdesmaths.fr'
            to_email = eleve_email
            try:
                send_mail(subject, body, from_email, [to_email,], fail_silently=False)
                qcmseq.mail_envoye = True
                qcmseq.save()
                count += 1
            except Exception, error:
                failed.append(qcmseq.id)
                log.error(str(error))

        msg = u"Nombre d'emails envoyés: %d. " % count
        unsent_qcmseqs = self.__get_unsent_qcm_queryset()
        if unsent_qcmseqs.count() == 0:
            msg += u"Il ne reste plus de QCM à envoyer."
            return HttpResponse(msg)
        else:
            msg += u"Il reste encore %d QcmSequences non envoyés. Identifiants: " % unsent_qcmseqs.count()
            msg += ', '.join([str(pk) for pk in unsent_qcmseqs.values_list('id', flat=True)])
            return HttpResponseServerError(msg)

    def post(self, request, *args, **kwargs):
        """
        This POST method is used to generate QCM Sequences for all students
        for a given theme.
        """

        # Check admin authorization
        if not check_admin_token(request):
            return HttpResponseForbidden("Droits insuffisants")

        try:
            # Read the body text from request
            json_body = json.loads(request.body.decode('utf-8'))
            theme_id = int(json_body['theme'])
            expiration = json_body['expiration']

            if theme_id <= 0:
                return HttpResponseBadRequest("Vous n'avez pas choisi de theme")

            # Find all students studying that theme
            theme = Theme.objects.get(pk=theme_id)
            classe = theme.cours.classe
            profiles = Profile.objects.filter(classe=classe)

            # Only commit if generation was successful for all students
            with transaction.atomic():
                count = 0
                for profile in profiles:

                    # Get the student's level
                    current_medaille = get_medaille_level(profile.user.id, theme_id)
                    niveau = 0 if not current_medaille else current_medaille

                    # Pick random QCMs among the available series suited for the student's level
                    random_qcms_dict = generate_random_qcm_list(theme, niveau=niveau)

                    # Create the QCM Sequence
                    qcm_seq = QcmSequence()
                    qcm_seq.theme = theme
                    qcm_seq.eleve = profile.user
                    qcm_seq.qcm_list = json.dumps(random_qcms_dict)
                    qcm_seq.nom = u"QCM de Prof: %s" % (theme)
                    qcm_seq.expiration = datetime.strptime(expiration, "%Y-%m-%d %H:%M:%S")
                    qcm_seq.save()
                    count += 1

            return HttpResponse(u"QCMs crées pour les %d élèves de %s" % (count, classe))

        except Exception, error:
            response = "Erreur: %s" % error
            log.error(response)
            return HttpResponseBadRequest(response)


@method_decorator(csrf_exempt, name='dispatch')
class MedailleView(View):

    def get(self, request, *args, **kwargs):
        # Check authorization
        user = get_token_user(request)
        if not user:
            return HttpResponseForbidden("Droits insuffisants")

        # Dict will contain for each cours a list of themes mapped to the medal
        cours = {}

        # Get all user medals
        medailles = dict(Medaille.objects.values_list('theme__id', 'niveau').filter(eleve=user))

        # Get all distinct themes that the student had to do
        theme_ids = QcmSequence.objects.values_list('theme').distinct().filter(eleve=user)
        themes = Theme.objects.filter(id__in=theme_ids)
        for theme in themes:
            theme_id = theme.id
            theme_name = theme.nom
            cours_name = theme.cours.nom
            if cours_name not in cours.keys():
                cours[cours_name] = dict()
            cours[cours_name][theme_name] = medailles[theme_id] if theme_id in medailles.keys() else 0

        results = {
            'partGlobal': "%.1f" % get_participation_rate(user.id),
            'partMonth': "%.1f" % get_participation_rate(user.id, since_days=30),
            'medailles': cours
        }

        return JsonResponse(results, safe=False)
