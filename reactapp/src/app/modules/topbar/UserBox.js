import React from "react";
import {Link} from 'react-router';
import {auth} from 'app/tools/Auth';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import {getSmallSpinner} from 'app/modules/common.js';
import "app/app.css";

class UserBox extends React.Component {

    constructor(props) {
        super(props);
        this.state = {sending: false};
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.submitLogin = this.submitLogin.bind(this);
        this.submitLogout = this.submitLogout.bind(this);
        this.getDisplayName = this.getDisplayName.bind(this);
    }
    
    static contextTypes = {
        router: React.PropTypes.object.isRequired
    };

    handleFieldChange(e) {
        if (e.target.type == "text") {
            this.formUsername = e.target.value;
        } else if (e.target.type == "password") {
            this.formPassword = e.target.value;
        }
    }

    submitLogin(e) {
        e.preventDefault();
        this.setState({sending: true});
        let username = this.formUsername;
        let password = this.formPassword;
        let callback = (token) => {
            if (token) {
                auth.loadUserData((data) => {
                    if (this.context.router.location.state) {
                        // On the "forbidden" page, send to the "next" target page
                        this.context.router.push(this.context.router.location.state.nextFullPath);
                    } else {
                        // On any page, simply reload
                        this.context.router.push(this.context.router.location.pathname);
                    }
                    this.setState({sending: false});
                });
            } else {
                this.setState({sending: false});
                alert("Erreur d'authentification");
            }
        };
        auth.login(username, password, callback);
    }

    submitLogout(e) {
        e.preventDefault();
        auth.logout();
        this.setState({username: null});
        this.context.router.push("/?logout=true");
    }

    getDisplayName(userData) {
        if (!userData) {
            return "";
        }
        if (userData.first_name && userData.last_name) {
            return userData.first_name + " " + userData.last_name;
        } else if (userData.first_name) {
            return userData.first_name;
        } else {
           return userData.username;
        }
    }

    render() {
        if (auth.loggedIn()) {
            let userData = auth.getUserData();            
            return (
                <li className="dropdown userbox">
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span className="glyphicon glyphicon-user"></span>&nbsp;&nbsp; Compte
                    </a>
                    <ul className="dropdown-menu">
                        <li className="dropdown-header">{this.getDisplayName(userData)}</li>
                        <li role="separator" className="divider"></li>
                        <li><Link to="/profil"><span className="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp; Mes Infos</Link></li>
                        {userData.is_superuser &&
                            <li><Link to="/admin-qcm-importer"><span className="glyphicon glyphicon-save"></span>&nbsp;&nbsp; Importer Exercises</Link></li>
                        }
                        {/*{userData.is_superuser &&*/}
                            {/*/!*<li><Link to="/admin-qcm-generer"><span className="glyphicon glyphicon-export"></span>&nbsp;&nbsp; Générer un QCM</Link></li>*!/*/}
                        {/*}*/}
                        <li><a href="#logout" onClick={this.submitLogout}><span className="glyphicon glyphicon-off"></span>&nbsp;&nbsp; Déconnexion</a></li>
                    </ul>
                </li>
            );
        } else {
            return (
                <li className="dropdown userbox">
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span className="glyphicon glyphicon-lock"></span>&nbsp;&nbsp; Login
                    </a>
                    <ul className="dropdown-menu">
                        <li>
                            <form className="navbar-form">
                                <div className="form-group">
                                    <input type="text" placeholder="Utilisateur" id="login" className="form-control" onChange={this.handleFieldChange} />
                                    <input type="password" placeholder="Mot de Passe" id="mdp" className="form-control" onChange={this.handleFieldChange} />
                                </div>
                                <div className="login-button-container">
                                    {this.state.sending ?
                                        getSmallSpinner()
                                    :
                                        <button type="submit" className="btn btn-common full-width" onClick={this.submitLogin}>Connexion</button>
                                    }
                                </div>
                            </form>
                        </li>
                    </ul>
                </li>
            );
        }
    }
}

export default UserBox;