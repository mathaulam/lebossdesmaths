import React, { Component } from 'react';
import McqQuestion from './McqQuestion';
import McqResults from './McqResults';
import {httpClient} from 'app/tools/HttpClient';
import {preprocessHtml, shuffleArray, getRandomInt, getErrorMsg} from 'app/tools/Helpers.js';
import {getSmallSpinner} from 'app/modules/common.js';

class McqSequence extends Component {

    constructor(props) {
        super(props);
        this.state = {sending: false, responseSuccess: false, responseFailure: false, finished: false};
        this.nextAction = this.nextAction.bind(this);
        this.setVerifiedIcons = this.setVerifiedIcons.bind(this);
        this.uploadSequenceAnswers = this.uploadSequenceAnswers.bind(this);
        this.questions = this.prepareQuestions(this.props.qcms);
    }

    componentDidMount() {
        // Re-init Mathjax after html has been rendered
        MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
        $('body').latex();
    }

    componentDidUpdate() {
        // Re-init Mathjax after html has been rendered
        MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
        $('body').latex();
    }

    prepareQuestions(qcms) {
        var questionSequence = [];
        qcms.map((qcm, index) => {

            let initialChoices = {
                1: preprocessHtml(qcm.reponse1), 
                2: preprocessHtml(qcm.reponse2), 
                3: preprocessHtml(qcm.reponse3), 
                4: preprocessHtml(qcm.reponse4)
            }

            // Generate an object containing choices in a random order
            let shuffledChoices = {};
            let correctAnswer = null;
            for (let i = 1; i <= 4; i++) {
                // Pick an available key randomly from the initial choices
                let randomInitialIndex = getRandomInt(0, Object.keys(initialChoices).length);
                let randomInitialKey = Object.keys(initialChoices)[randomInitialIndex];
                // Store the value corresponding to this random key to the new object
                shuffledChoices[i] = {
                    text: initialChoices[randomInitialKey],
                    origIndex: parseInt(randomInitialKey, 10)
                }
                // Remember where the 4th value was placed, it's the correct answer
                if (randomInitialKey == 4) {
                    correctAnswer = i;
                }
                // Remove the value from the initial choices and continue with remaining values
                delete initialChoices[randomInitialKey];
            }

            questionSequence[index] = {
                id: qcm.id,
                question: preprocessHtml(qcm.question),
                choices: shuffledChoices,
                answer: correctAnswer
            };
        });

        return questionSequence;
    }

    uploadSequenceAnswers() {

        if (!this.props.seqId) {
            // Previsualizing or training, just pretend save is OK
            this.setState({sending: false, responseMsg: "", responseSuccess: true, finished: true});
            this.setVerifiedIcons();
            return;
        }

        this.setState({sending: true});
        this.props.changeStep(-1);
        let origIndexChoices = {};
        for (var question of this.questions) {
            let chosenOrigIndex = -1;
            if (question.chosen > 0) {
                chosenOrigIndex = question.choices[question.chosen].origIndex
            }
            origIndexChoices[question.id] = chosenOrigIndex;
        }

        httpClient.post(
            "/api/qcm-seq/?seqId=" + this.props.seqId,
            origIndexChoices,
            (resp) => {
                this.setState({sending: false, responseMsg: resp.data, responseSuccess: true, finished: true});
                this.setVerifiedIcons();
            },
            (error) => {
                let errorMsg = getErrorMsg(error);
                this.setState({sending: false, responseMsg: errorMsg.substring(0, 500), responseFailure: true});
            }
        );
    }

    setVerifiedIcons() {
        for (let i = 0; i < 5; i++) {
            let status = this.questions[i].chosen == this.questions[i].answer ? "correct" : "wrong";
            this.props.setIconStatus(i, status);
        }
    }

    nextAction(index) {
        if (this.props.currentStep < this.questions.length - 1) {
            // Move to next question
            this.props.changeStep(this.props.currentStep + 1);
        } else {
            // Finish the sequence and upload answers
            this.props.changeStep(-1);
        }
    }

    render() {
        if (this.props.currentStep >= 0) {
            // Displaying current question
            return (
                <McqQuestion 
                    questionObj={this.questions[this.props.currentStep]}
                    nextAction={this.nextAction}
                    currentStep={this.props.currentStep}
                    setIconStatus={this.props.setIconStatus}
                    finished={this.state.finished}
                />
            );

        } else if (this.state.sending) {
            // Display spinner while results are being uploaded
            return getSmallSpinner();
        } else if (this.state.responseSuccess) {
            // Display the results page if answers have been successfully uploaded
            return <McqResults questions={this.questions} message={this.state.responseMsg}/>
        } else if (this.state.responseFailure) {
            // Display error and button to retry
            return (
                <div>
                    <h3>Error durant la sauvegarde des résultats:</h3>
                    <div style={{border: "1px solid #666", padding: 10, fontSize: "0.8em"}}>
                        {this.state.responseMsg}
                    </div>
                    <div style={{textAlign: "center"}}>
                        <input type="submit" value="Réessayer" onClick={this.uploadSequenceAnswers} />
                    </div>
                </div>
            )
        } else {
            // Display last page with button to send results
            return (
                <div className="text-center">
                    <h3>Valider les réponses</h3>
                    {this.props.seqId && 
                    <p>Cliquez sur le bouton ci-dessous pour valider et sauvegarder vos réponses.</p>
                    }
                    <p>Attention, assurez-vous d'avoir répondu à toutes les questions !</p>
                    <button type="button" onClick={this.uploadSequenceAnswers} className="btn btn-info">Valider</button>
                </div>
            )
        }
    }
}

export default McqSequence;
