import React from "react";
import {httpClient} from 'app/tools/HttpClient';
import BasePage from "./BasePage";
import {Link} from 'react-router';
import TopNavBar from "app/modules/topbar/TopNavBar";
import {preprocessHtml, getErrorMsg} from 'app/tools/Helpers.js';
import "app/pages/pages.css";

class Sublanding extends BasePage {

    constructor(props) {
        super(props);
        this.state = {exam:''};
        this.loadData = this.loadData.bind(this);
        this.loadData();
    }

    loadData() {
        let examId = this.props.params.id;

        // Get the exam page details.
        httpClient.get(
            "/api/sublanding/" + examId,
            (resp) => this.setState({exam: resp.data}),
            (error) => alert(getErrorMsg(error))
        );
    }

    componentDidUpdate() {
        // Re-init Mathjax after html has been rendered
        MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
        $('body').latex();
    }


    renderBody() {
        
        if (!this.state.exam) {
            return this.showLoader();
        }

        return (
            <div>
                <section className="title-section">
                    <div className="container">
                        <div className="row">
                            <h1>{this.state.exam.name}</h1>
                            {
                                 this.state.exam.paragraphs.map((paragraph, i) => {
                                    return (
                                        <div className="sublanding_paragraph" key={i}>
                                            <h2>{paragraph.title}</h2>
                                            <p>{paragraph.content}</p>
                                            <Link to={"boutique/"+this.state.exam.id}>View offer</Link>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Sublanding;