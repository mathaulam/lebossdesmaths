import React from "react";
import {Link} from 'react-router';
import ReactPlayer from 'react-player'
import {httpClient} from 'app/tools/HttpClient';
import BasePage from "./BasePage";
import TopNavBar from "app/modules/topbar/TopNavBar";
import {preprocessHtml, getErrorMsg} from 'app/tools/Helpers.js';
import { browserHistory } from 'react-router'
import "app/pages/pages.css";

class Epreuve extends BasePage {

    constructor(props) {
        super(props);
        this.state = {epreuve: null, reponses: null, validating: false, nonuser: true};
        this.loadData = this.loadData.bind(this);
        this.validate = this.validate.bind(this);
        this.loadData();
    }

    loadData() {
        let epreuveId = this.props.params.id;
        let eleveId = this.props.location.query.eleveId;
        let epreuveUrl = "/api/epreuves/?id=" + epreuveId;

        if (eleveId) {
            // In case of admin previewing student results
            epreuveUrl += "&eleveId=" + eleveId;
        }

        // Get name of classe
        httpClient.get(
            epreuveUrl,
            (resp) => {
                this.setState({epreuve: resp.data, validating: false, nonuser: false});
            },
            (error) => {
                if (error.response.status !== '403'){
                    window.location.href = "/app/class_registration/";
                }
            },
        );

    }

    componentDidUpdate() {
        // Re-init Mathjax after html has been rendered
        MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
        $('body').latex();
    }

    validate() {
        let epreuveId = this.props.params.id;

        // Find answers from DOM elements
        let answers = {};
        $('.epreuve-question').each(function() {
            let id = $(this).attr('data-qid');
            if ($(this).prop("tagName") == 'SELECT') {
                answers[id] = $('option:selected', this).index();
            } else if ($(this).prop("tagName") == 'INPUT') {
                answers[id] = $(this).val();
            } else if ($(this).attr("data-qtype") == 'radio') {
                // Inside radio group div
                let radioVal = $('input[name=' + id + ']:checked', this).val();
                answers[id] = radioVal !== undefined ? radioVal : 0;
            } else if ($(this).attr("data-qtype") == 'checkbox') {
                // Inside checkbox group div
                answers[id] = $('input[name=' + id + ']', this).is(':checked') ? 1 : 0;
            }
        });

        // Save answers to API
        httpClient.post(
            "/api/epreuves/?id=" + epreuveId,
            answers,
            (resp) => {
                this.setState({validating: true});
                this.loadData();
            },
            (error) => alert(getErrorMsg(error))
        );
    }


    renderQuestions() {
        return (
            <div>
                <section className="title-section">
                    <div className="container">
                        <div className="row">
                            <h1>{this.state.epreuve.cours}</h1>
                        </div>
                    </div>
                </section>
                <div className="container">
                    <div className="row">
                        <div className="cours-contents">
                            <div dangerouslySetInnerHTML={{__html: preprocessHtml(this.state.epreuve.contenu)}} />
                            <button type="submit" className="btn button-animated" onClick={this.validate}>
                                <b>Valider les réponses</b>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderResults() {
        return (
            <div>
                <section className="title-section">
                    <div className="container">
                        <div className="row">
                            <h2>Epreuve sur {this.state.epreuve.cours}</h2>
                        </div>
                    </div>
                </section>
                <div className="container">
                    <div className="row">
                        <div className="cours-contents">
                            <div className="panel panel-primary">
                                <div className="panel-heading">
                                    Résultats de l'épreuve
                                </div>
                                <div className="panel-body">
                                    Vous avez obtenu un score de <b>{this.state.epreuve.result.score_eleve}</b> / {this.state.epreuve.result.score_max}.
                                    <br/>Vous pouvez relire les questions pour voir quelles étaient les bonnes réponses.
                                </div>
                            </div>
                            <div dangerouslySetInnerHTML={{__html: preprocessHtml(this.state.epreuve.contenu)}} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderBody() {

        if (!this.state.epreuve || this.state.validating) {
            return this.showLoader();
        }


        if (this.state.epreuve.result) {
            return this.renderResults();
        } else {
            return this.renderQuestions();
        }
        
    }
}

export default Epreuve;