# Webpack2 + React App

Sample app demonstrating the power and simplicity of React, Redux, and Webpack 2. Includes tree shaking configuration. 
Based on this project: https://github.com/ModusCreateOrg/budgeting-sample-app-webpack2

## Setup

```
$ npm install
```

## Running in dev mode using local Django as backend

```
$ npm start
```

## Running in dev mode using npm mocked REST as backend

```
$ npm run dev
```

## Running in production mode

```
$ NODE_ENV='production' npm start
```

## Build (production)

```
$ NODE_ENV='production' npm run build
```