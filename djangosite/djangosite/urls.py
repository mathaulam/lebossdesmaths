"""djangosite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import RedirectView
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token
from react_app.views import ReactAppView
from classes.views import ClasseViewSet
from cours.views import CoursViewSet, ThemeViewSet, EpreuveView, ProgrammesViewSet, BlocsViewSet, GrainsViewSet, \
    LessonViewSet
from users.views import UserViewSet, UserProfileView, ExamUserView
from landing_pages.views import HomepageView, SublandingViewSet
from qcms.views import QcmViewSet, QcmSequenceView, QcmAdminImportView, \
    QcmAdminGenerateView, MedailleView
from djangosite import settings

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'utilisateurs', UserViewSet)
router.register(r'examusers', ExamUserView)
router.register(r'grains', GrainsViewSet)
router.register(r'classes', ClasseViewSet)
router.register(r'cours', CoursViewSet)
router.register(r'sublanding', SublandingViewSet)
router.register(r'themes', ThemeViewSet)
router.register(r'programmes', ProgrammesViewSet)
router.register(r'blocs', BlocsViewSet)
router.register(r'qcms', QcmViewSet)
router.register(r'lessons', LessonViewSet)

urlpatterns = [
    # Auth urls
    url(r'^api/api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/obtain-auth-token$', obtain_auth_token),

    # Custom views
    url(r'^api/admin-qcm-import/$', QcmAdminImportView.as_view()),
    url(r'^api/admin-qcm-generate/$', QcmAdminGenerateView.as_view()),
    url(r'^api/user-profile/$', UserProfileView.as_view()),
    url(r'^api/qcm-seq/$', QcmSequenceView.as_view()),
    url(r'^api/medailles/$', MedailleView.as_view()),
    url(r'^api/epreuves/$', EpreuveView.as_view()),
    url(r'^api/homepage/$', HomepageView.as_view(), name='homepage_details'),
    #url(r'^api/sublanding/$', SublandingList.as_view(), name='sublanding_list'),



    # REST Framework urls
    url(r'^api/', include(router.urls)),

    # Django plugins
    url(r'^admin/', admin.site.urls),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

    # React front-end app
    url(r'^app/', ReactAppView.as_view()),
    url(r'^$', RedirectView.as_view(url='/app', permanent=False), name='index'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
