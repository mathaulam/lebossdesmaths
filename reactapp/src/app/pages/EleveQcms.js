import React from "react";
import BasePage from "./BasePage";
import TopNavBar from "app/modules/topbar/TopNavBar";
import {Link} from 'react-router';
import {auth} from 'app/tools/Auth';
import {httpClient} from 'app/tools/HttpClient';
import {getErrorMsg} from 'app/tools/Helpers.js';
import "app/pages/pages.css";

class EleveQcms extends BasePage {

    constructor(props) {
        super(props);
        this.state = {qcmSeqs: null, themeList: null, themeValue: -1};
        this.loadQcms = this.loadQcms.bind(this);
        this.loadThemeList = this.loadThemeList.bind(this);
        this.handleThemeChange = this.handleThemeChange.bind(this);
        this.loadQcms();
        this.loadThemeList();
    }

    loadQcms() {
        let coursId = this.props.params.id;

        // Get name of classe
        httpClient.get(
            "/api/qcm-seq/?userId=" + auth.getUserData().id,
            (resp) => this.setState({qcmSeqs: resp.data}),
            (error) => alert(getErrorMsg(error))
        );
    }

    loadThemeList() {
        // Get list of theme
        httpClient.get("/api/themes",
            (resp) => this.setState({themeList: resp.data}),
            (error) => alert(getErrorMsg(error))
        );
    }

    getQcmTheme(qcmSeqObj) {
        let parts = qcmSeqObj.theme.fullname.split(' > ');
        parts.shift(); // Remove the class name
        return <span className="small" data-id={qcmSeqObj.id}>{parts.join(' > ')}</span>
    }

    getQcmStatus(qcmSeqObj) {
        if (qcmSeqObj.score != null) {
            return <span className="small">Score: <b>{qcmSeqObj.score}</b></span>
        } else {
            let expiration = new Date(qcmSeqObj.expiration);
            let expIso = expiration.toISOString();
            let date = expIso.substring(0, 10);
            let time = expIso.substring(11,13) + "h" + expIso.substring(14,16)
            let msg = "À faire avant le " + date + " à " + time;
            return <div className="small">{msg}</div>;
        }
    }

    getQcmIcon(qcmSeqObj) {
        if (qcmSeqObj.score != null) {
            return <span className="small glyphicon glyphicon-ok"></span>;
        } else if (new Date(qcmSeqObj.expiration) < new Date()) {
            return <span className="small glyphicon glyphicon-remove"></span>;
        } else {
            return (
                <Link to={"/qcm?seqId=" + qcmSeqObj.id}>
                    <span className="glyphicon glyphicon-play" style={{color: '#60b040'}}></span>
                </Link>
            );
        }
    }

    handleThemeChange(event) {
        this.setState({themeValue: event.target.value});
    }

    renderQcmList() {
        return (
            <table className="eleve-qcm-table">
                <thead>
                <tr>
                    <th>Thème</th>
                    <th>Statut</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.qcmSeqs.map((qcmSeqObj, i) => 
                        (
                            <tr key={qcmSeqObj.id}>
                                <td>{this.getQcmTheme(qcmSeqObj)}</td>
                                <td>{this.getQcmStatus(qcmSeqObj)}</td>
                                <td>{this.getQcmIcon(qcmSeqObj)}</td>
                            </tr>
                        )
                    )
                }
                </tbody>
            </table>
        );
    }

    renderBody() {

        if (!this.state.qcmSeqs || !this.state.themeList) {
            return this.showLoader();
        }

        return (
            <div>
                {this.state.qcmSeqs && this.state.qcmSeqs.length > 0 &&
                <div>
                    <section className="title-section">
                        <div className="container">
                            <div className="row">
                                <h2>QCMs notés</h2>
                                <p>
                                    Ces qcms sont générés par le site en fonction de votre niveau actuel.
                                    <br />
                                    Attention, chaque QCM ne peut être fait qu'une seule fois, et les résultats sont enregistrés.
                                </p>
                            </div>
                        </div>
                    </section>
                    <div className="container">
                        <div className="qcm-seq-list-wrapper">
                            {this.renderQcmList()}
                        </div>
                    </div>
                </div>
                }
                <div>
                    <section className="title-section">
                        <div className="container">
                            <div className="row">
                                <h2>QCMs d'entraînement</h2>
                                <p>
                                    Vous pouvez ici générer des QCMs aléatoires.
                                    <br />
                                    Libre à vous de vous entraîner si vous le souhaitez, les résultats ne sont pas enregistrés.
                                </p>
                            </div>
                        </div>
                    </section>
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12">
                                <label htmlFor="theme">Thème:</label>
                                <select id="theme" value={this.state.themeValue} onChange={this.handleThemeChange} className="form-control">
                                    <option value="-1">-----</option>
                                    { this.state.themeList && 
                                        this.state.themeList.map((object, i) => {
                                            return (
                                                <option key={object.id} value={object.id}>{object.fullname}</option>
                                            );
                                        })
                                    }
                                </select>
                            </div>
                            <div className="col-xs-12">
                                {this.state.themeValue > 0 &&
                                <Link to={"/qcm?themeId=" + this.state.themeValue} className="btn button-animated full-width">
                                    <b>Démarrer !</b>
                                </Link>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default EleveQcms;