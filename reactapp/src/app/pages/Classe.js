import React from "react";
import {Link} from 'react-router';
import {httpClient} from 'app/tools/HttpClient';
import BasePage from "./BasePage";
import TopNavBar from "app/modules/topbar/TopNavBar";
import {getErrorMsg, getRandomInt} from 'app/tools/Helpers.js';
import "app/pages/pages.css";

class Classe extends BasePage {

    constructor(props) {
        super(props);
        this.state = {classe:null, coursList:null};
        this.loadData = this.loadData.bind(this);
        this.loadData(this.props.params.id);

        this.illustrations = [
            '/img/illustrations/ballistique.png',
            '/img/illustrations/eleve_calco.png',
            '/img/illustrations/eleve_reflechit.png',
            '/img/illustrations/eleve_trace_cercle.png',
            '/img/illustrations/mesure_carre.png',
        ]
    }

    componentWillReceiveProps(nextProps) {
        this.setState({classe:null, coursList:null});
        this.loadData(nextProps.params.id);
    }

    loadData(classeId) {

        // Get name of classe
        httpClient.get(
            "/api/classes/" + classeId,
            (resp) => {
                this.setState({classe: resp.data});
                this.loadCours(classeId);
            },
            (error) => alert(getErrorMsg(error))
        );
    }

    loadCours(classeId) {
        // Get list of cours
        httpClient.get(
            "/api/classes/" + classeId + "/cours",
            (resp) => this.setState({coursList: resp.data}),
            (error) => alert(getErrorMsg(error))
        );
    }

    getRandomIllustration() {
        let index = getRandomInt(0, this.illustrations.length);
        let url = this.illustrations[index];
        return <img src={url} alt="Illustration" className="img-responsive center-block"/>;
    }

    renderBody() {
        
        if (!this.state.coursList) {
            return this.showLoader();
        }
        return (
            <div>
                <section className="title-section">
                    <div className="container">
                        <div className="row">
                            <h1>{this.state.classe.nom}</h1>
                            {/* <h2>Liste des Cours</h2> */}
                        </div>
                    </div>
                </section>
                <div className="container cours-list">
                    <div className="row">
                        <div className="col-xs-12 col-sm-8 col-lg-6 vcenter">
                            <section className="todo">
                                <ul className="todo-list">
                                    <li>Liste des Cours de {this.state.classe.name} :</li>
                                {
                                    this.state.coursList.map((object, i) => {
                                        return (
                                            <li key={object.id}>
                                                - <Link to={ '/cours/' + object.id}>{object.nom}</Link>
                                            </li>
                                        )
                                    })
                                }
                                </ul>
                            </section>
                        </div>
                        <div className="col-xs-12 col-sm-4 col-lg-6 vcenter text-center">
                            {this.getRandomIllustration()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Classe;