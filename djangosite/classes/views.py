from rest_framework import viewsets
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from common.helpers import check_admin_token
from classes.serializers import ClasseSerializer
from cours.serializers import CoursMinimalSerializer, BlockSerializer, CoursSerializer
from cours.models import Cours, Grain, Block, Lesson
from classes.models import Classe


class ClasseViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows classes to be viewed or edited.
    """
    queryset = Classe.objects.all()
    serializer_class = ClasseSerializer

    @detail_route(methods=['get'])
    def cours(self, request, pk=None):
        cours = Cours.objects.filter(classe_id=pk)
        serializer = CoursMinimalSerializer(cours, many=True, context={'request':request})
        return Response(serializer.data)

    @detail_route(methods=['get'])
    def cours_full(self, request, pk=None):
        cours = Cours.objects.filter(classe_id=pk)
        serializer = CoursSerializer(cours, many=True, context={'request':request})
        return Response(serializer.data)



