import React from "react";
import TopNavBar from "app/modules/topbar/TopNavBar";
import BasePage from "./BasePage";
import {auth} from 'app/tools/Auth';
import {getSmallSpinner} from 'app/modules/common.js';
import {httpClient} from 'app/tools/HttpClient';
import {getErrorMsg} from 'app/tools/Helpers.js';
import "app/pages/pages.css";

class UserProfile extends BasePage {
        
    static contextTypes = {
        router: React.PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {sending: false};
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.submitInfo = this.submitInfo.bind(this);
    }

    handleFieldChange(e) {
        this.setState({[e.target.id]: e.target.value});
    }

    submitInfo(e) {
        e.preventDefault();
        this.setState({sending: true});

        httpClient.post(
            "/api/user-profile/", 
            this.state,
            (resp) => {
                this.setState({sending: false, responseMsg: resp.data, responseSuccess: true});
                // Re-load user data and re-render whole page so that the name can be updated in the top UserBox. 
                auth.loadUserData((data) => this.context.router.push(this.context.router.location.pathname));
            },
            (error) => {
                let errorMsg = getErrorMsg(error);
                this.setState({sending: false, responseMsg: errorMsg.substring(0, 500), responseSuccess: false});
            }
        );

    }

    renderBody() {
        const userData = auth.getUserData();
        return (
            <div>
                <section className="title-section">
                    <div className="container">
                        <div className="row">
                            <h1>Mes Infos</h1>
                        </div>
                    </div>
                </section>
                <div className="container">
                    <div className="row profile-info-contents">
                        <form>
                            <div className="form-group">
                                <label htmlFor="email">Courriel</label>
                                <input type="email" className="form-control" id="email" 
                                    defaultValue={userData.email} onChange={this.handleFieldChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="first_name">Prénom</label>
                                <input type="text" className="form-control" id="first_name" 
                                    defaultValue={userData.first_name} onChange={this.handleFieldChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="last_name">Nom</label>
                                <input type="text" className="form-control" id="last_name" 
                                defaultValue={userData.last_name} onChange={this.handleFieldChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="classe">Classe</label>
                                <input type="text" className="form-control" id="classe" defaultValue={userData.profile.classe && userData.profile.classe.nom} disabled={true}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="programme">Programme</label>
                                <input type="text" className="form-control" id="programme" defaultValue={userData.profile.programme && userData.profile.programme.name} disabled={true}/>
                            </div>
                            {this.state.sending ?
                                getSmallSpinner()
                            :
                                <button type="submit" className="btn button-animated full-width" onClick={this.submitInfo}><b>Enregistrer</b></button>
                            }
                        </form>
                        {this.state.responseMsg && this.state.responseSuccess &&
                            <div className="alert alert-info">{this.state.responseMsg}</div>
                        }
                        {this.state.responseMsg && !this.state.responseSuccess &&
                            <div className="alert alert-danger">{this.state.responseMsg}</div>
                        }
                    </div>
                </div>
            </div>
        );
    }

}

export default UserProfile;