from django.contrib.sites.models import Site
from django.test import TestCase, RequestFactory, override_settings
from django.urls import reverse

from .views import HomepageView
from .models import Homepage, Paragraph, SublandingPage


class TestViews(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.site = Site.objects.create(
            domain='test.com',
            name='test site',
        )
        p = Paragraph.objects.create(
            title='first_paragraph',
            main_image='ss',
            content='this is test for paragraph'
        )
        self.settings = Homepage.objects.create()
        self.settings.paragraphs.add(p)
        self.settings.sites.add(self.site)

    @override_settings(SITE_ID=2)
    def test_get_homepage_settings(self):
        """Assert that homepage settings are returned successfully."""
        # Test setup
        request = self.client.get(reverse('homepage_details'))
        # Test body
        # Test assertion
        print(request)
        self.assertEqual(request.status_code, 200)
        self.assertIsNotNone(request.data['paragraphs'])


    def test_get_exams_list(self):
        """Assert that exams list will be returned from the API."""
        # Test setup
        SublandingPage.objects.create(name='exam1')
        request = self.client.get(reverse('sublanding_list'))
        # Test body
        # Test assertion
        print(request)
        self.assertEqual(request.status_code, 200)

    def test_get_sublanding_detail(self):
        """Assert that sublanding page details is retrieved successfully."""
        # Test setup
        sublanding_test = SublandingPage.objects.create(name='examss1')
        request = self.client.get(reverse('sublanding_detail', kwargs={'page_id': sublanding_test.pk}))
        # Test body
        # Test assertion
        print(request)
        self.assertEqual(request.status_code, 200)