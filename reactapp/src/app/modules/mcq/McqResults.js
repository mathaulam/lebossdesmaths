import React, { Component } from 'react';
import {Link} from 'react-router';

class McqResults extends Component {
    
    constructor(props) {
        super(props);
        this.getTotalScore = this.getTotalScore.bind(this);
        this.goToEleveQcmPage = this.goToEleveQcmPage.bind(this);
    }

    static contextTypes = {
        router: React.PropTypes.object.isRequired
    };

    componentDidUpdate() {
        // Re-init Mathjax after html has been rendered
        MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
    }

    goToEleveQcmPage(e) {
        e.preventDefault();
        this.context.router.push("/eleve-qcms");
    }

    getTotalScore() {
        let score = 0;
        this.props.questions.map((element, index) => {
            if (element.answer == element.chosen) {
                score++;
            }
        });
        return score;
    }
    
    render() {
        return (
            <div className="mcqResults text-center">                
                <h4>
                    Vous avez obtenu un score de : <b>{this.getTotalScore()}</b> / 5
                </h4>
                {this.getTotalScore() < 5 ?
                    <p>Vous pouvez retourner sur les questions entourées de <span className="error-color">rouge</span> ci-dessus pour voir quelles étaient les bonnes réponses.</p>
                :
                    <p>Bravo !</p>
                }
                
                {this.props.message &&
                <div className="alert alert-warning" role="alert">{this.props.message}</div>
                }
                <button type="button" onClick={this.goToEleveQcmPage} className="btn btn-info next-step">
                    Retour Page QCMs
                </button>
            </div>
        )
    }
}

export default McqResults;
