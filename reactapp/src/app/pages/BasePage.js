import React from "react";
import TopNavBar from "app/modules/topbar/TopNavBar";
import "app/pages/pages.css";

class BasePage extends React.Component {

    showLoader() {
        return(
            <div className="container container-loading">
                <img src="/static/img/gears.svg" className="center-block" />
            </div>
        );
    }

    render() {
        return (
            <div>
                <header id="header">
                    <TopNavBar />
                </header>

                {this.renderBody()}
                
                <footer id="footer">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-12 text-center bottom-separator">
                                <img src="/img/layout/under.png" className="img-responsive inline" alt="" />
                            </div>
                            <div className="col-sm-12">
                                <div className="text-center">
                                    <p className="copyright">&copy; LaBossDesMaths 2017. Tous droits réservés.</p>
                                    <p className="credits">
                                        Réalisation par <a target="_blank" href="http://blog.ditullio.fr">NDT</a> |
                                        Design par <a target="_blank" href="http://www.websecudev.com">Websecudev</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

export default BasePage;