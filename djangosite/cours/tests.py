from django.test import TestCase

from model_mommy import mommy

from cours.models import Lesson, Grain, Exercise, GrainInLesson


class LessonModelTest(TestCase):
    
    def test_get_random_exercises(self):
        """Assert that model will return specific amount of random related exercises."""
        # Test setup
        lesson = mommy.make(Lesson)
        grains = mommy.make(Grain, _quantity=3)
        for grain in grains:
            GrainInLesson.objects.create(
                lesson=lesson,
                grain=grain,
                exercises=5
            )
            exams = mommy.make(Exercise, _quantity=20)
            for exam in exams:
                grain.exercises.add(exam)

        # Test body
        results = lesson.choose_random_exercises()

        # Test assertion
        self.assertEqual(len(results), 3)
        grain_ids = []
        for grain in grains:
            grain_ids.append(grain.id)
        self.assertEqual(len(results[grain_ids[0]]), 5)
        self.assertEqual(len(results[grain_ids[1]]), 5)
        self.assertEqual(len(results[grain_ids[2]]), 5)

    def test_get_random_exercises_with_number_gt_actual(self):
        """Assert that model will handle it if admin specifies a number > actual exercises number."""
        # Test setup
        lesson = mommy.make(Lesson)
        grains = mommy.make(Grain, _quantity=3)
        for grain in grains:
            GrainInLesson.objects.create(
                lesson=lesson,
                grain=grain,
                exercises=0
            )

        # Test body
        results = lesson.choose_random_exercises()

        # Test assertion
        self.assertEqual(len(results), 0)

