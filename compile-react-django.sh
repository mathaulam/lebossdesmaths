#!/bin/bash

########
# This script compiles react and transfers the bundles and resources to the django project.
##

REACT_PROJECT="reactapp"
DJANGO_PROJECT="djangosite"
DJANGO_REACT_APP="$DJANGO_PROJECT/react_app"
TIMESTAMP=$(date +%s)

# Clean all generated data that might still exist
rm -rf $DJANGO_REACT_APP/static/img/*
rm -rf $DJANGO_REACT_APP/static/js/*
rm -rf $DJANGO_PROJECT/static_root/*
rm -rf $REACT_PROJECT/dist/*

# If argument "clean" was passed, exit now right after cleaning phase
if [ "$1" == "clean" ]; then
    exit 0
fi

# Compile bundles and copy resouces to the 'react/dist' folder
(cd $REACT_PROJECT && NODE_ENV='production' npm run build)

# Copy js bundles
cp -r $REACT_PROJECT/dist/js $DJANGO_REACT_APP/static
echo "Copied js bundles"

# Copy images
cp -r $REACT_PROJECT/dist/img $DJANGO_REACT_APP/static
echo "Copied images"

# Copy index html
cp $REACT_PROJECT/dist/index.html $DJANGO_REACT_APP/templates/index.html
#  Prepending '/static' path to bundle files to be served by nginx
sed -i 's|src="/js/|src="/static/js/|g' $DJANGO_REACT_APP/templates/index.html
# Append query param with current datetime for cache-busting
sed -i "s|\.bundle\.js|\.bundle\.js?t=$TIMESTAMP|g" $DJANGO_REACT_APP/templates/index.html
echo "Generated index.html"

# Static files need to be collected after being copied to app
echo "Collecting static files"
python $DJANGO_PROJECT/manage.py collectstatic --noinput
echo "Finished !"
