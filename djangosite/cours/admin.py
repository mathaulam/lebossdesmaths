from django.contrib import admin
from django.utils.html import format_html
from cours.models import *


class EpreuveAdmin(admin.ModelAdmin):
    search_fields = ['cours__nom', 'cours__classe__nom']


class GrainAdmin(admin.ModelAdmin):
    search_fields = ['lesson__name',]


class ExecriseInIntraBlock(admin.TabularInline):
    model = ExerciseInIntraBloc
    extra = 2


class GrainsInline(admin.TabularInline):
    model = GrainInLesson
    extra = 3


@admin.register(Lesson)
class LessonAdmin(admin.ModelAdmin):
    inlines = (GrainsInline,)


@admin.register(IntrablocTest)
class IntraTestAdmin(admin.ModelAdmin):
    filter_horizontal = ('lessons',)
    inlines = (ExecriseInIntraBlock,)


@admin.register(InterblocTest)
class IntraTestAdmin(admin.ModelAdmin):
    pass


class ComponentsInline(admin.TabularInline):
    model = BlockComponent
    extra = 3

@admin.register(Block)
class BlockAdmin(admin.ModelAdmin):
    inlines = (ComponentsInline, )


@admin.register(Programme)
class ProgrammeAdmin(admin.ModelAdmin):
    filter_horizontal = ('blocks',)


class ReponseEpreuveAdmin(admin.ModelAdmin):
    list_display = ['id', 'show_previsu', 'eleve', 'epreuve']
    search_fields = ['epreuve__cours__nom', 'eleve__first_name', 'eleve__last_name']
    readonly_fields = ['reponses']

    def show_previsu(self, obj):
        return format_html('<a href="/app/epreuves/{epreuve}?eleveId={eleve}" target="_blank">Previsu</a>',
                           epreuve=obj.epreuve.id, eleve=obj.eleve.id)
    show_previsu.short_description = "Visionner"


class CoursAdmin(admin.ModelAdmin):
    search_fields = ['nom', 'classe__nom']


class ThemeAdmin(admin.ModelAdmin):
    search_fields = ['nom']


admin.site.register(Cours, CoursAdmin)
admin.site.register(Theme, ThemeAdmin)
admin.site.register(Epreuve, EpreuveAdmin)
admin.site.register(Grain, GrainAdmin)
admin.site.register(ReponseEpreuve, ReponseEpreuveAdmin)
