# -*- coding: utf-8 -*-
from qcms.models import QcmSequence, Medaille
from datetime import datetime, timedelta


def get_participation_rate(user_id, since_days=None):
    # Query the QcmSequences
    qcmseqs = QcmSequence.objects.values_list('score', flat=True).filter(eleve_id=user_id)
    if since_days:
        since_date = datetime.today() - timedelta(days=since_days)
        qcmseqs = qcmseqs.filter(created__gt=since_date)

    # Calculate the rate
    total = qcmseqs.count()
    done = 0
    for score in qcmseqs:
        if score is not None:
            done += 1
    if total == 0:
        return 0
    else:
        return float(done) * 100 / total


def get_medaille_level(user_id, theme_id):
    query = Medaille.objects.filter(eleve_id=user_id, theme_id=theme_id)
    return query.get().niveau if query.count() > 0 else None


def set_medaille(user_id, theme_id, niveau):
    query = Medaille.objects.filter(eleve_id=user_id, theme_id=theme_id)
    if query.count() > 0:
        medaille = query.get()
        medaille.niveau = niveau
        medaille.save()
    else:
        medaille = Medaille()
        medaille.eleve_id = user_id
        medaille.theme_id = theme_id
        medaille.niveau = niveau
        medaille.save()


def get_score_history_for_theme(user_id, theme_id):
    qcmseqs = QcmSequence.objects.values_list('score', flat=True).filter(eleve_id=user_id)
    return qcmseqs


def upgrade_medaille(user_id, theme_id, score):
    current_medaille = get_medaille_level(user_id, theme_id)

    # No medal yet
    if not current_medaille or current_medaille == 0:
        if score >= 3:
            # Get Bronze
            set_medaille(user_id, theme_id, 1)
            return u"Vous avez obtenu la médaille de Bronze !"

    # From Bronze
    if current_medaille == 1:
        if score == 4:
            # Get Silver
            set_medaille(user_id, theme_id, 2)
            return u"Vous avez obtenu la médaille d'Argent !"
        elif score == 5:
            # Get Gold
            set_medaille(user_id, theme_id, 3)
            return u"Vous avez obtenu la médaille d'Or !"
        elif score < 3:
            # Loose medal:
            set_medaille(user_id, theme_id, 0)
            return u"Vous avez perdu votre médaille de Bronze."

    # From Silver
    if current_medaille == 2:
        if score == 5:
            # Get Gold
            set_medaille(user_id, theme_id, 3)
            return u"Vous avez obtenu la médaille d'Or !"
        elif score < 3:
            # Go down to Bronze
            set_medaille(user_id, theme_id, 2)
            return u"Vous avez perdu votre médaille d'Argent. Vous avez maintenant le Bronze."

    # From Gold
    if current_medaille == 3:
        if score == 5:
            # Get Platinium
            set_medaille(user_id, theme_id, 4)
            return u"Vous avez obtenu la médaille de Platine ! Elle ne peut être perdue. Bravo !"
        elif score < 3:
            # Go down to Silver
            set_medaille(user_id, theme_id, 2)
            return u"Vous avez perdu votre médaille d'Or. Vous avez maintenant l'Argent."

    # No result message if medal status has not changed
    return ""
