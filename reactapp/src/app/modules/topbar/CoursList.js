import React from "react";
import {httpClient} from 'app/tools/HttpClient';
import {Link} from 'react-router';
import {auth} from 'app/tools/Auth';
import {getErrorMsg} from 'app/tools/Helpers.js';

class CoursList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {data: []};
        this.loadData = this.loadData.bind(this);
        this.loadData();
    }

    loadData() {
        httpClient.get(
            "/api/classes",
            (resp) => this.setState({data: resp.data}),
            (error) => alert(getErrorMsg(error))
        );
    }

    render() {

        return (
            <ul className="dropdown-menu">
                {auth.getUserData() && auth.getUserData().profile.classe && <li><Link to={ '/classes/' + auth.getUserData().profile.classe.id}><b>{auth.getUserData().profile.classe.nom}</b></Link></li>}
                {auth.getUserData() && auth.getUserData().profile.classe && <li role="separator" className="divider"></li>}
                {
                    this.state.data.map((object, i) => {
                        return (
                            <li key={object.id}>
                                <Link to={ '/classes/' + object.id}>{object.nom}</Link>
                            </li>
                        )
                    })
                }
            </ul>
        );
    }
}

class UnsedCode{
    ShowUserClasseUnsedCode()   {
        //const showUserClasse = auth.loggedIn() && auth.getUserData().profile && auth.getUserData().profile.classe;

    }
}
export default CoursList;