import React from "react";
import TopNavBar from "app/modules/topbar/TopNavBar";
import BasePage from "app/pages/BasePage";
import "app/pages/pages.css";

class Forbidden extends BasePage {
    
    render() {
            return (
                <div>
                    <TopNavBar />
                    <div className="container">
                        <div className="jumbotron">
                            <h2>Erreur Serveur.</h2>
                            <hr />
                            <p>Une erreur s'est produite.</p>
                        </div>
                    </div>
                </div>
            );
        }

}

export default Forbidden;