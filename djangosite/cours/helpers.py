# -*- coding: utf-8 -*-
from simpleeval import safe_power, SimpleEval
from unidecode import unidecode
from HTMLParser import HTMLParser
import math
import difflib
import json
import re
import ast

# Create a new SimpleEval instance to set "^" operator as power
# and defines extra math functions
evaluator = SimpleEval()
evaluator.operators[ast.BitXor] = safe_power
evaluator.functions['exp'] = lambda x: math.exp(x)
evaluator.functions['ln'] = lambda x: math.log(x)
evaluator.functions['log'] = lambda x: math.log10(x)

#################
# HTML TEMPLATES
#################

HTML_BOX_START = '\
<div class="panel panel-%s"> \
    <div class="panel-heading"> \
        <div class="panel-title">%s</div> \
    </div> \
    <div class="panel-body">'

HTML_BOX_END = '</div></div>'

HTML_PDF = '<iframe \
src="https://drive.google.com/viewerng/viewer?url=%s?pid=explorer&efh=false&a=v&chrome=false&embedded=true" \
width="%spx" height="%spx"></iframe>'

HTML_YOUTUBE = '<iframe \
src="https://www.youtube.com/embed/%s?modestbranding=1&autohide=1&showinfo=0" \
width="%spx" height="%spx" frameborder="0" allowfullscreen></iframe>'


def find_attribute_value(attributes, name):
    matches = re.findall(name + '={(.*?)}', attributes, re.I)
    return matches[0] if matches else None


#########################
# BASIC COURS PSEUDOTAGS
#########################

def prepare_latex_tags(text):
    text = text.replace('[latex]', '<script type="tex/latex">')
    text = text.replace('[/latex]', '</script>')
    return text


def prepare_box_tags(text):

    def get_panel_style_and_title_by_box_type(box_type):
        if box_type:
            if box_type.lower() == "definition":
                return ("info", u"Définition")
            elif box_type.lower() == "exemple":
                return ("success", u"Exemple")
            elif box_type.lower() == "theoreme":
                return ("danger", u"Théorème")
            elif box_type.lower() == "methode":
                return ("warning", u"Méthode")
            elif box_type.lower() == "remarque":
                return ("default", u"Remarque")
        return ("default", "")

    # Replace tag openings (and first line return if there is one) with div start
    box_pattern = re.compile('\[box(.*)\](<br \/>)?', re.I)
    for match in re.finditer(box_pattern, text):
        full_opening_tag = match.group(0)
        attributes = match.group(1)
        attr_type = find_attribute_value(attributes, 'type')
        attr_titre = find_attribute_value(attributes, 'titre')
        css_class, default_title = get_panel_style_and_title_by_box_type(attr_type)
        title = attr_titre if attr_titre else default_title
        html_box_start = HTML_BOX_START % (css_class, title)
        text = text.replace(full_opening_tag, html_box_start)

    # Replace all tag closings with div end
    text = text.replace('[/box]', HTML_BOX_END)

    return text


def prepare_pdf_tags(text):

    pdf_pattern = re.compile('\[pdf(.*)\]', re.I)
    for match in re.finditer(pdf_pattern, text):
        full_tag = match.group(0)
        attributes = match.group(1)
        attr_lien = find_attribute_value(attributes, 'lien')
        attr_largeur = find_attribute_value(attributes, 'largeur')
        attr_hauteur = find_attribute_value(attributes, 'hauteur')
        html_pdf = HTML_PDF % (attr_lien, attr_largeur, attr_hauteur)
        text = text.replace(full_tag, html_pdf)

    return text


def prepare_youtube_tags(text):

    pdf_pattern = re.compile('\[youtube(.*)\]', re.I)
    for match in re.finditer(pdf_pattern, text):
        full_tag = match.group(0)
        attributes = match.group(1)
        attr_id = find_attribute_value(attributes, 'id')
        attr_largeur = find_attribute_value(attributes, 'largeur')
        attr_hauteur = find_attribute_value(attributes, 'hauteur')
        html_youtube = HTML_YOUTUBE % (attr_id, attr_largeur, attr_hauteur)
        text = text.replace(full_tag, html_youtube)

    return text


def process_pseudotags(text):
    text = prepare_latex_tags(text)
    text = prepare_box_tags(text)
    text = prepare_pdf_tags(text)
    text = prepare_youtube_tags(text)
    return text


##########################
# TEXT COMPARISON HELPER
##########################

def replace_html_chars(text):
    parser = HTMLParser()
    return parser.unescape(text)


def compare_text(a, b):

    # Make sure the text are unicode
    if not isinstance(a, unicode):
        a = unicode(a, 'utf-8')
    if not isinstance(b, unicode):
        b = unicode(b, 'utf-8')

    # Replace HTML encoded chars
    a = replace_html_chars(a)
    b = replace_html_chars(b)

    # Remove diacritics and lower the case for better matching
    a = unidecode(a).lower()
    b = unidecode(b).lower()

    # Calculate diff
    s = difflib.SequenceMatcher(None, a, b)
    return s.ratio()


###############################
# EPREUVE QUESTIONS PSEUDOTAGS
###############################


def prepare_questions(text):

    # Process standard pseudotags first
    text = process_pseudotags(text)

    # Loop through all question tags
    question_tags_pattern = re.compile('\[question(.*)\]', re.I)
    for match in re.finditer(question_tags_pattern, text):
        full_tag = match.group(0)
        attributes = match.group(1)

        # Find ID with optional double quotes
        attr_id = find_attribute_value(attributes, 'id')
        attr_type = find_attribute_value(attributes, 'type')
        attr_reponse = find_attribute_value(attributes, 'reponse')

        html_field = ''
        if not attr_id or not attr_type or not attr_reponse:
            html_field = '<span style="color:red">ERREUR</span>'
        elif attr_type.lower() == 'trou':
            html_field = '<input class="epreuve-question question_input form-input" data-qid="%s" type="text" />' % attr_id
        elif attr_type.lower() == 'choix':
            html_field = '<select class="epreuve-question form-select" data-qid="%s">' % attr_id
            parmi = find_attribute_value(attributes, 'parmi')
            html_field += '<option>-</option>'
            for option in parmi.split('|'):
                html_field += '<option>%s</option>' % option
            html_field += '</select>'
        elif attr_type.lower() == 'radio':
            parmi = find_attribute_value(attributes, 'parmi')
            html_field = '<div class="epreuve-question custom-radiobtn" data-qid="%s" data-qtype="radio">' % attr_id
            index = 0
            for option in parmi.split('|'):
                index += 1
                html_field += '<label class="radio-container">'
                html_field += '<input type="radio" name="%s" value="%s" /> %s<br />' % (attr_id, index, option)
                html_field += '<span class="checkmark"></span>'
                html_field += '</label>'
            html_field += '</div>'
        elif attr_type.lower() == 'checkbox':
            html_field = '<div class="custom-checkbox">'
            html_field += '<span class="epreuve-question" data-qid="%s" data-qtype="checkbox">' % attr_id
            html_field += '<label class="checkbox-container">'
            html_field += '<input type="checkbox" name="%s" />' % (attr_id)
            html_field += '<span class="checkmark"></span>'
            html_field += '</label>'
            html_field += '</span>'
            html_field += '</div>'

        text = text.replace(full_tag, html_field)

    return text


def validate_answers(text, answers):
    """
    Validates answers submitted by the student.
    Returns a dictionary in the form:
    {
        '1': {'reponse': '2x+5', 'ok': True},
        '2': {'reponse': '1', 'ok': True},
        '3': {'reponse': 'Je ne sais pas', 'ok': False},
    }
    """
    # Dictionary to store results and points
    results = {}

    # Parse the student's answers to dict
    eleve_reponses = json.loads(answers)

    # Loop through all question tags
    tag_attributes = re.findall('\[question(.*)\]', text, re.I)
    for attributes in tag_attributes:

        # Find ID with optional double quotes
        attr_id = find_attribute_value(attributes, 'id')
        attr_type = find_attribute_value(attributes, 'type')
        attr_reponse = find_attribute_value(attributes, 'reponse')
        # Get student's answer
        eleve_reponse = eleve_reponses[attr_id]

        if not attr_id or not attr_type or not attr_reponse:
            continue

        best_ratio = -1
        if attr_type.lower() == 'trou':
            # TEXT INPUT QUESTION
            results[attr_id] = {'reponse': eleve_reponse, 'ok': False}
            index = 0
            for reponse in attr_reponse.split('|'):
                index += 1
                attr_mode = find_attribute_value(attributes, 'mode')
                if attr_mode == 'eval':
                    eval_eleve = 0
                    eval_prof = 0
                    try:
                        eval_eleve = evaluator.eval(eleve_reponse)
                    except:
                        results[attr_id]['eval'] = 'ERREUR ELEVE'

                    try:
                        eval_prof = evaluator.eval(reponse)
                    except:
                        results[attr_id]['eval'] = 'ERREUR PROF: REPONSE #%s' % index

                    if eval_eleve == eval_prof:
                        results[attr_id]['index_reponse'] = index
                        results[attr_id]['ok'] = True

                else:
                    # If not eval type, just consider it as text.
                    # If no ratio was set, set ratio to 1, i.e. exact match
                    attr_ratio = find_attribute_value(attributes, 'ratio')
                    min_ratio = float(attr_ratio) if attr_ratio else 1
                    calculated_ratio = compare_text(eleve_reponse, reponse)
                    if calculated_ratio > best_ratio:
                        # If best ratio so far, save to the result
                        best_ratio = calculated_ratio
                        results[attr_id]['ratio'] = calculated_ratio
                        if calculated_ratio >= min_ratio:
                            # If better than min threshold, mark as success
                            best_ratio = calculated_ratio
                            results[attr_id]['index_reponse'] = index
                            results[attr_id]['ok'] = True

        elif attr_type.lower() == 'choix':
            # SELECT DROPDOWN QUESTION
            repondu_index = int(eleve_reponse)
            results[attr_id] = {'reponse': repondu_index, 'ok': False}
            if int(eleve_reponse) == int(attr_reponse):
                results[attr_id]['ok'] = True

        elif attr_type.lower() == 'radio':
            # RADIO BUTTONS QUESTION
            repondu_index = int(eleve_reponse)
            results[attr_id] = {'reponse': repondu_index, 'ok': False}
            if int(eleve_reponse) == int(attr_reponse):
                results[attr_id]['ok'] = True

        elif attr_type.lower() == 'checkbox':
            # CHECKBOX QUESTION
            results[attr_id] = {'reponse': int(eleve_reponse), 'ok': False}
            if int(eleve_reponse) == int(attr_reponse):
                results[attr_id]['ok'] = True

        else:
            # TYPE NOT FOUND, MARK ERROR IN DB
            results[attr_id] = {
                'ERREUR': 'Type de question %s non trouve.' % attr_type,
                'ok': False
            }

    return results


def complete_answered_tags(text, reponses):
    reponse_dict = json.loads(reponses)

    # Process standard pseudotags first
    text = process_pseudotags(text)

    # Loop through all question tags
    question_tags_pattern = re.compile('\[question(.*)\]', re.I)
    for match in re.finditer(question_tags_pattern, text):
        full_tag = match.group(0)
        attributes = match.group(1)

        # Find ID with optional double quotes
        attr_id = find_attribute_value(attributes, 'id')
        attr_type = find_attribute_value(attributes, 'type')
        attr_reponse = find_attribute_value(attributes, 'reponse')

        html_result = ''

        if not attr_id or not attr_type or not attr_reponse:
            html_result = '<span style="color:red; font-weight:bold;">ERREUR</span>'

        elif attr_type.lower() == 'trou':
            if reponse_dict[attr_id]['ok']:
                html_result = u'<input type="text" class="question_input correct" value="%s" disabled />' % reponse_dict[attr_id]['reponse']
            else:
                correct_answer = attr_reponse.replace('|', ' ou ')
                repondu = reponse_dict[attr_id]['reponse']
                if repondu:
                    html_result = u'<input type="text" class="question_input wrong" value="%s" disabled />' % repondu
                else:
                    html_result = u'<span class="wrong">(non répondu)</span>'
                html_result += u' =&gt; <span class="correction">%s</span>' % correct_answer

        elif attr_type.lower() == 'choix':
            parmi = find_attribute_value(attributes, 'parmi').split('|')
            if reponse_dict[attr_id]['ok']:
                reponse = parmi[reponse_dict[attr_id]['reponse']-1]
                html_result = u'<select class="correct" disabled><option>%s</option></select>' % reponse
            else:
                correct_answer = parmi[int(attr_reponse)-1]
                repondu_index = reponse_dict[attr_id]['reponse']
                repondu_text = parmi[repondu_index-1] if repondu_index > 0 else '-'
                html_result = u'<select class="wrong" disabled><option>%s</option></select>' % repondu_text
                html_result += u' =&gt; <span class="correction">%s</span>' % correct_answer

        elif attr_type.lower() == 'radio':
            repondu_index = int(reponse_dict[attr_id]['reponse'])
            parmi = find_attribute_value(attributes, 'parmi')
            html_result = '<div>'
            index = 0
            for option in parmi.split('|'):
                index += 1
                is_checked = 'checked' if index == repondu_index else ''
                color_class = ''
                if int(attr_reponse) == index:
                    color_class = 'correct'
                elif int(attr_reponse) != index and is_checked:
                    color_class = 'wrong'
                html_result += u'<span class="question_checkbox %s"><input type="radio" disabled %s/></span> %s<br />' % (color_class, is_checked, option)
            html_result += '</div>'

        elif attr_type.lower() == 'checkbox':
            color_class = 'correct' if reponse_dict[attr_id]['ok'] else 'wrong'
            is_checked = 'checked' if int(reponse_dict[attr_id]['reponse']) == 1 else ''
            html_result = u'<span class="question_checkbox %s"><input type="checkbox" disabled %s/></span>' % (color_class, is_checked)

        text = text.replace(full_tag, html_result)

    return text


def get_score_result(reponses):
    reponse_dict = json.loads(reponses)
    score_max = 0
    score_eleve = 0
    for key, val in reponse_dict.iteritems():
        score_max += 1
        if val['ok']:
            score_eleve += 1
    return {'score_max': score_max, 'score_eleve': score_eleve}
