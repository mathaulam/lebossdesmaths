import React from "react";
import {httpClient} from 'app/tools/HttpClient';
import BasePage from "./BasePage";
import TopNavBar from "app/modules/topbar/TopNavBar";
import RegistrationForm from "app/modules/topbar/RegistrationForm";
import {preprocessHtml, getErrorMsg} from 'app/tools/Helpers.js';
import "app/pages/pages.css";

class Boutique extends BasePage {

    constructor(props) {
        super(props);
        this.state = {exam:''};
        this.loadData = this.loadData.bind(this);
        this.loadData();
    }

    loadData() {
        let examId = this.props.params.exam_id;

        // Get the exam page details.
        httpClient.get(
            "/api/sublanding/" + examId,
            (resp) => this.setState({exam: resp.data}),
            (error) => alert(getErrorMsg(error))
        );
    }

    componentDidUpdate() {
        // Re-init Mathjax after html has been rendered
        MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
        $('body').latex();
    }


    renderBody() {
        
        if (!this.state.exam) {
            return this.showLoader();
        }
        return (
            <div>
                <section className="title-section">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-sm-8 col-lg-6">
                                <RegistrationForm programme_id={this.state.exam.related_programme} />
                            </div>                        
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Boutique;