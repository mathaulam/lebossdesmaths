from classes.models import Classe
from rest_framework import serializers

from cours.models import Programme


class ClasseSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Classe
        fields = ['id', 'nom']


class ProgrammeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Programme
        fields = ['id', 'name']

