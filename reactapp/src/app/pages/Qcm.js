import React from "react";
import {Link} from 'react-router';
import {httpClient} from 'app/tools/HttpClient';
import BasePage from "./BasePage";
import TopNavBar from "app/modules/topbar/TopNavBar";
import McqWizard from "app/modules/mcq/McqWizard";
import {auth} from "app/tools/Auth";
import {getErrorMsg} from 'app/tools/Helpers.js';
import "app/pages/pages.css";

class Qcm extends BasePage {

    constructor(props) {
        super(props);
        this.state = {qcms: null};
        this.loadData = this.loadData.bind(this);
        this.loadData();
    }

    loadData() {
        let seqId = this.props.location.query.seqId;
        let previsuId = this.props.location.query.previsuId;
        let themeId = this.props.location.query.themeId;

        if (seqId) {
            httpClient.get(
                "/api/qcm-seq?seqId=" + seqId,
                (resp) => {
                    this.setState({qcms: resp.data});
                },
                (error) => {
                    alert(getErrorMsg(error));
                }
            );

        } else if (themeId) {
             httpClient.get(
                "/api/qcm-seq?themeId=" + themeId,
                (resp) => {
                    this.setState({qcms: resp.data});
                },
                (error) => {
                    alert(getErrorMsg(error));
                }
            );

        } else if (previsuId) {
            httpClient.get(
                "/api/qcms/" + previsuId,
                (resp) => this.setState({qcms: Array(1).fill(resp.data)}),
                (error) => {
                    if (error.response && error.response.status == 403) {
                        alert("Veuillez vous authentifier en tant qu'administrateur sur la partie admin.")
                    } else {
                        alert(getErrorMsg(error));
                    }
                }
            );
        }
    }

    renderBody() {
        if (!this.state.qcms) {
            return this.showLoader();
        }

        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="panel panel-info">
                            <div className="panel-heading">
                                QCM
                            </div>
                            <div className="panel-body">
                                {this.state.qcms.length > 0 ?
                                    <McqWizard qcms={this.state.qcms} seqId={this.props.location.query.seqId}/>
                                :
                                    <div>QCM inexistant.</div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Qcm;