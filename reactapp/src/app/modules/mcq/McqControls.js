import React, { Component } from 'react';
import './Mcq.css';

class McqControls extends Component {

    constructor(props) {
        super(props);
        this.state = {icons: []};
        this.goToStep = this.goToStep.bind(this);
    }

    goToStep(e, step) {
        e.preventDefault();
        this.props.changeStep(step);
    }

    renderQuestionIcon(iconStep) {
        const status = this.props.icons[iconStep];
        return (
            <li role="presentation" className={iconStep == this.props.currentStep ? "active" : ""}>
                <a href={"#step" + iconStep} onClick={(e) => this.goToStep(e, iconStep)}>
                    <span className={"round-tab " + (status ? status : "")}>
                        { iconStep+1 }
                    </span>
                </a>
            </li>
        );
    }

    renderResultsIcon() {
        return (
            <li role="presentation" className={-1 == this.props.currentStep ? "active" : ""}>
                <a href="#results" onClick={(e) => this.goToStep(e, -1)}>
                    <span className="round-tab">
                        <i className="glyphicon glyphicon-ok"></i>
                    </span>
                </a>
            </li>
        );
    }

    render() {
        return (
            <div className="wizard-inner">
                <div className="connecting-line"></div>
                <ul className="nav nav-tabs" role="tablist">
                    {this.renderQuestionIcon(0)}
                    {this.renderQuestionIcon(1)}
                    {this.renderQuestionIcon(2)}
                    {this.renderQuestionIcon(3)}
                    {this.renderQuestionIcon(4)}
                    {this.renderResultsIcon()}
                </ul>
            </div>
        )
    }
}

export default McqControls;
