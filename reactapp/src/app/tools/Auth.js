import axios from "axios";
import {appConfig} from "app/tools/AppConfig";

class Auth {
    
    login(username, password, callback) {
        if (localStorage.token) {
            if (callback) {
                callback(true);
            }
            return;
        }

        let params = {username: username, password: password};
        axios.post(appConfig.getServerUrl() + '/api/obtain-auth-token', params)
        .then((res) => {
            localStorage.token = res.data.token;
            if (callback) {
                callback(res.data.token);
            }
        })
        .catch((error) => {
            if (callback) {
                callback(false);
            }
        });
    }   
    
    logout() {
        delete localStorage.userData;
        delete localStorage.token;
    }

    loggedIn() {
        return !!localStorage.token && !!localStorage.userData;
    }
    
    loadUserData(callback) {
        let config = {
            headers: {"Authorization": "Token " + localStorage.token}
        };
        axios.get(appConfig.getServerUrl() + '/api/utilisateurs/me', config)
        .then((res) => {
            localStorage.userData = JSON.stringify(res.data);
            if (callback) {
                callback(res.data);
            }
        })
        .catch((error) => {
            if (callback) {
                callback(false);
            }
        });
    }

    getUserData() {
        if (localStorage.userData) {
            return JSON.parse(localStorage.userData);
        }
        return null;
    }
    
}

export let auth = new Auth();
