from django.contrib.auth.models import User
from rest_framework import serializers
from users.models import Profile
from classes.serializers import ClasseSerializer, ProgrammeSerializer


class ProfileSerializer(serializers.ModelSerializer):

    classe = ClasseSerializer()
    programme = serializers.SerializerMethodField()

    def get_programme(self, obj):
        return ProgrammeSerializer(obj.get_current_programme()).data

    class Meta:
        model = Profile
        fields = ('classe', 'programme', 'pseudo_classe')


class UserSerializer(serializers.ModelSerializer):

    id = serializers.ReadOnlyField()
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'email', 'first_name',
                  'last_name', 'is_superuser', 'profile')

    def update(self, instance, validated_data):

        # Update the User with editable values
        instance.email = validated_data.get('email', instance.email)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.save()

        return instance


class ExamUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'first_name', 'last_name')
