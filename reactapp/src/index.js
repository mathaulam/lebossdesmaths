import "babel-polyfill";
import React from "react";
import {render} from "react-dom";
import {Router, Route, Redirect} from 'react-router';
import { createHistory } from 'history'
import { useRouterHistory } from 'react-router'
import TopNavBar from "app/modules/topbar/TopNavBar";
import Accueil from "app/pages/Accueil";
import Classe from "app/pages/Classe";
import Lesson from "app/pages/Lesson";
import Cours from "app/pages/Cours";
import Sublanding from "app/pages/Sublanding";
import Boutique from "app/pages/Boutique";
import StaticRegistration from "app/pages/StaticRegistration";
import Epreuve from "app/pages/Epreuve";
import EleveQcms from "app/pages/EleveQcms";
import EleveMedailles from "app/pages/EleveMedailles";
import Qcm from "app/pages/Qcm";
import UserProfile from "app/pages/UserProfile";
import AdminQcmImport from "app/pages/AdminQcmImport";
import AdminQcmGenerate from "app/pages/AdminQcmGenerate";
import Forbidden from "app/pages/errors/Forbidden";
import ServerError from "app/pages/errors/ServerError";
import NotFound from "app/pages/errors/NotFound";
import Poc from "app/pages/Poc";
import {auth} from "app/tools/Auth";

class App extends React.Component {

    constructor(props) {
        super(props);
    }

    requireAuth(nextState, replaceState) {
        if (!auth.loggedIn()) {
            const fullNextPath = nextState.location.pathname + nextState.location.search;
            replaceState({ 
                pathname:'/interdit',
                state: {nextFullPath: fullNextPath}
            })
        }
    }

    forVisitors(nextState, replaceState) {
        if (auth.loggedIn()) {
            // const fullNextPath = nextState.location.pathname + nextState.location.search;
            replaceState({
                pathname:'/profil',
                // state: {nextFullPath: fullNextPath}
            })
        }
    }

    requireAdmin(nextState, replaceState) {
        if (!(auth.loggedIn() && auth.getUserData().is_superuser)) {
            replaceState({ 
                pathname:'/interdit'
            })
        } 
    }

    render() {
        const browserHistory = useRouterHistory(createHistory)({
            basename: '/app'
        })
        return (
            <Router history={browserHistory}>
                <Route path="/" component={Accueil} />
                <Route path="/classes/:id" component={Classe} />
                <Route path="/lessons/:id" component={Lesson} />
                <Route path="/cours/:id" component={Cours} />
                <Route path="/sublanding/:id" component={Sublanding} onEnter={this.forVisitors} />
                <Route path="/boutique/:exam_id" component={Boutique} onEnter={this.forVisitors} />
                <Route path="/class_registration/" component={StaticRegistration} onEnter={this.forVisitors} />
                <Route path="/epreuves/:id" component={Epreuve} />
                <Route path="/eleve-qcms" component={EleveQcms} onEnter={this.requireAuth} />
                <Route path="/eleve-medailles" component={EleveMedailles} onEnter={this.requireAuth} />
                <Route path="/qcm" component={Qcm} onEnter={this.requireAuth} />
                <Route path="/profil" component={UserProfile} onEnter={this.requireAuth} />
                <Route path="/admin-qcm-importer" component={AdminQcmImport} onEnter={this.requireAdmin} />
                <Route path="/admin-qcm-generer" component={AdminQcmGenerate} onEnter={this.requireAdmin} />
                <Route path="/interdit" component={Forbidden} />
                <Route path='/erreur' component={ServerError} />
                <Route path='/introuvable' component={NotFound} />
                <Route path='/poc' component={Poc} />
                <Redirect from='*' to='/introuvable' />
            </Router>
        );
    }
}

render(<App/>, window.document.getElementById("root"));