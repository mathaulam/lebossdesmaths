from rest_framework import serializers

from .models import Homepage, Paragraph, SublandingPage


class ParagraphSerializer(serializers.ModelSerializer):

    class Meta:
        model = Paragraph
        fields = ('id', 'title', 'main_image', 'content', 'link')


class HomepageSerializer(serializers.ModelSerializer):

    paragraphs = ParagraphSerializer(many=True)

    class Meta:
        model = Homepage
        fields = ('paragraphs',)


class SublandingListSerializer(serializers.ModelSerializer):

    paragraphs = ParagraphSerializer(many=True)

    class Meta:
        model = SublandingPage
        fields = '__all__'