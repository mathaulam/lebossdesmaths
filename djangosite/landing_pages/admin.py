from django.contrib import admin

from preferences.admin import PreferencesAdmin
from .models import Homepage, Paragraph, SublandingPage, Email


# Register your models here.
@admin.register(Homepage)
class ContactSettingsAdmin(PreferencesAdmin):
    exclude = ['sites']


@admin.register(Email)
class EmailSettingsAdmin(PreferencesAdmin):
    exclude = ['sites']

@admin.register(Paragraph)
class ParagraphAdmin(admin.ModelAdmin):
    pass


@admin.register(SublandingPage)
class SublandingPageAdmin(admin.ModelAdmin):
    pass