from __future__ import unicode_literals
from cours.helpers import process_pseudotags
from django.contrib.auth.models import User
from django.db import models

from classes.models import Classe

from django_mysql.models import ListCharField
from ckeditor_uploader.fields import RichTextUploadingField


class Cours(models.Model):
    classe = models.ForeignKey(Classe)
    ordre = models.PositiveSmallIntegerField()
    nom = models.CharField(max_length=255)
    video = models.URLField(blank=True)
    contenu = RichTextUploadingField(blank=True)
    bilan = RichTextUploadingField(blank=True)
    methodes = RichTextUploadingField(blank=True)
    exercices = RichTextUploadingField(blank=True)
    prive = models.BooleanField(default=False)

    @property
    def processed(self):
        return {
            'contenu': process_pseudotags(self.contenu),
            'bilan': process_pseudotags(self.bilan),
            'methodes': process_pseudotags(self.methodes),
            'exercices': process_pseudotags(self.exercices),
        }

    @property
    def linked_epreuves(self):
        epreuve_objs = Epreuve.objects.filter(cours__pk=self.pk).order_by('ordre')
        epreuve_ids = [obj.id for obj in epreuve_objs]
        return epreuve_ids

    class Meta:
        ordering = ['ordre', ]
        verbose_name_plural = 'Cours'

    def __unicode__(self):
        return u'%s > %s' % (self.classe, self.nom)


class Theme(models.Model):
    nom = models.CharField(max_length=255)
    cours = models.ForeignKey(Cours)

    def __unicode__(self):
        return u'%s > %s' % (self.cours, self.nom)


class Epreuve(models.Model):
    cours = models.ForeignKey(Cours)
    contenu = RichTextUploadingField(blank=True)
    ordre = models.PositiveSmallIntegerField(default=0)

    class Meta:
        ordering = ['ordre', ]

    def __unicode__(self):
        return u'Epreuve: %s' % (self.cours)


class ReponseEpreuve(models.Model):
    epreuve = models.ForeignKey(Epreuve)
    eleve = models.ForeignKey(User)
    reponses = models.CharField(max_length=1000)
    created = models.DateTimeField(auto_now_add=True)


class Grain(models.Model):
    """
    A model that represents the smallest fundamental piece of knowledge.
    """
    name = models.CharField(max_length=225)
    contenu = RichTextUploadingField(blank=True)
    summary = RichTextUploadingField(blank=True)
    ordre = models.PositiveSmallIntegerField(default=0)
    lesson = models.ForeignKey(
        to='cours.Lesson',
        related_name='lesson_grains',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    def __unicode__(self):
        return u'Grain: %s' % str(self.name)


class Lesson(models.Model):
    """
    A model that represents a collection of grains and exercises.
    """

    # Attributes
    name = models.CharField(max_length=100)
    grains = models.ManyToManyField(
        to='cours.Grain',
        related_name='lessons',
        through='GrainInLesson'
    )

    # Methods
    def choose_random_exercises(self):
        """
        Chooses random set of exercises based on difficulty and number of exams.
        :return: Dict of exercises.
        """
        exercises = {}
        try:
            for index, grain in enumerate(self.graininlesson_set.all()):
                # Get grains related to this lesson in order, attach a random exam_number excesses
                try:
                    if grain.exercises:
                        exercises[grain.grain.id] = grain.grain.exercises.all().order_by('?')[:grain.exercises]
                except Grain.DoesNotExist:
                    pass
        except AttributeError:
            pass

        return exercises

    def __unicode__(self):
        return u'%s' % (self.name)


class GrainInLesson(models.Model):
    """
    A model that wraps the each grain with its specified number of exercises.
    """
    lesson = models.ForeignKey(
        to='cours.Lesson',
        on_delete=models.CASCADE
    )
    grain = models.ForeignKey(
        to='cours.Grain',
        on_delete=models.CASCADE
    )
    exercises = models.PositiveSmallIntegerField(
        null=True,
        blank=True,
        verbose_name='Number of exercises'
    )


class Block(models.Model):
    """
    A model that represents the collection of grains and tests.
    """

    class Meta:
        verbose_name = 'Bloc'
        verbose_name_plural = 'Blocs'

    name = models.CharField(max_length=120)
    components = models.ManyToManyField(
        to='cours.Lesson',
        through='BlockComponent',
        related_name='blocks'
    )

    def __unicode__(self):
        return u'%s' % (self.name)


class BlockComponent(models.Model):
    """
    A model that wraps the grains and their related intrablocks tests.
    """
    lesson = models.ForeignKey(
        to='cours.Lesson',
        on_delete=models.CASCADE
    )
    block = models.ForeignKey(
        to='cours.Block',
        on_delete=models.CASCADE
    )
    intrabloc_test = models.ForeignKey(
        to='cours.IntrablocTest',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )


class IntrablocTest(models.Model):
    """
    A model that represents the tests that lay between blocs.
    """

    # Attributes
    name = models.CharField(max_length=100)
    type_of_exercises = models.ManyToManyField(
        to='qcms.ExerciseTypes',
        through='cours.ExerciseInIntraBloc',
    )
    lessons = models.ManyToManyField(
        to='cours.Lesson',
        related_name='intrabloc_tests'
    )

    def __unicode__(self):
        return u'%s' % (self.name)


class ExerciseInIntraBloc(models.Model):
    """
    A secondary model to represent the relation between test and its exercises.
    """

    class Meta:
        verbose_name = 'Exercise in Intrabloc test'

    # Attributes
    test = models.ForeignKey(
        to='cours.IntrablocTest',
        related_name='test'
    )
    type = models.ForeignKey(
        to='qcms.ExerciseTypes',
        related_name='exercises_in_intrablock'
    )
    number_of_exercises = models.PositiveSmallIntegerField(default=0)


class InterblocTest(models.Model):
    """
    A model that represents the tests that lay between grains.
    """

    # Attributes
    name = models.CharField(max_length=100)
    block_number = models.PositiveSmallIntegerField()
    program = models.ForeignKey(
        to='cours.Programme',
        related_name='interbloc_tests',
        null=True,
    )

    def __unicode__(self):
        return u'%s' % (self.name)


class Programme(models.Model):
    """
    A model that represents the collection of multiple blocks.
    """
    name = models.CharField(max_length=220)
    blocks = models.ManyToManyField(
        to='cours.Block',
        related_name='programmes'
    )

    def __unicode__(self):
        return u'%s' % (self.name)

