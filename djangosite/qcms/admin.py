from django.contrib import admin
from django.utils.html import format_html
from qcms.models import Qcm, QcmSequence, Medaille, GeneralExercise, ExerciseTypes


class QcmAdmin(admin.ModelAdmin):
    list_display = ['id', 'show_previsu', 'theme', 'niveau', 'serie', 'variante', 'question']
    search_fields = ['question', 'cours__nom', 'theme', 'serie', 'variante']

    def show_previsu(self, obj):
        return format_html('<a href="/app/qcm?previsuId={id}" target="_blank">Previsu</a>', id=obj.id)
    show_previsu.short_description = "Previsu"


class GeneralExeceriseAdmin(admin.ModelAdmin):
    list_display = ['id', 'grain', 'type', 'serie', 'variante', 'question']
    search_fields = ['question', 'type', 'serie', 'variante']


    # def show_previsu(self, obj):
    #     return format_html('<a href="/app/qcm?previsuId={id}" target="_blank">Previsu</a>', id=obj.id)
    # show_previsu.short_description = "Previsu"

@admin.register(ExerciseTypes)
class ExTypesAdmin(admin.ModelAdmin):
    list_display = ['id', 'type']


class QcmSequenceAdmin(admin.ModelAdmin):
    list_display = ['id', 'eleve', 'nom', 'score', 'expiration', 'created']
    search_fields = ['nom', 'eleve__username', 'eleve__first_name', 'eleve__last_name', 'theme__nom']


class MedailleAdmin(admin.ModelAdmin):
    list_display = ['id', 'eleve', 'theme', 'niveau']
    search_fields = ['eleve__username', 'eleve__first_name', 'eleve__last_name', 'theme__nom']


admin.site.register(Qcm, QcmAdmin)
admin.site.register(GeneralExercise, GeneralExeceriseAdmin)
admin.site.register(QcmSequence, QcmSequenceAdmin)
admin.site.register(Medaille, MedailleAdmin)