import React from "react";
import {httpClient} from 'app/tools/HttpClient';
import BasePage from "./BasePage";
import TopNavBar from "app/modules/topbar/TopNavBar";
import {getSmallSpinner} from 'app/modules/common.js';
import {getErrorMsg} from 'app/tools/Helpers.js';
import "app/pages/pages.css";

class AdminQcmImport extends BasePage {

    constructor(props) {
        super(props);
        this.state = {themeValue: '-1', importValue: '', sending: false, themeList: null, grain: null};
        this.handleThemeChange = this.handleThemeChange.bind(this);
        this.handleImportChange = this.handleImportChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.loadThemeList();
    }

    loadThemeList() {
        // Get list of theme
        httpClient.get("/api/grains",
            (resp) => this.setState({themeList: resp.data}),
            (error) => alert(getErrorMsg(error))
        );
    }

    handleThemeChange(event) {
        this.setState({grain: event.target.value});
    }

    handleImportChange(event) {
        this.setState({importValue: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({sending: true});

        // Send QCM data
        httpClient.post(
            "/api/admin-qcm-import/", 
            {grain: this.state.grain, data: this.state.importValue},
            (resp) => this.setState({sending: false, responseMsg: resp.data, responseSuccess: true}),
            (error) => {
                let errorMsg = getErrorMsg(error);
                this.setState({sending: false, responseMsg: errorMsg.substring(0, 500), responseSuccess: false});
            }
        );
    }

    renderBody() {

        return (
            <div>
                <section className="title-section">
                    <div className="container">
                        <div className="row">
                            <h1>Import de Exercises</h1>
                        </div>
                    </div>
                </section>
                <div className="container">
                    <div className="jumbotron">
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="theme">Choisissez le grain associé aux Exercises:</label>
                                <select id="theme" value={this.state.grain} onChange={this.handleThemeChange} className="form-control">
                                    <option value="-1">-----</option>
                                    { this.state.themeList && 
                                        this.state.themeList.map((object, i) => {
                                            return (
                                                <option key={object.id} value={object.id}>{object.name}</option>
                                            );
                                        })
                                    }

                                </select>
                            </div>
                            <div className="form-group">
                                <label htmlFor="import">Collez ici au format texte les Exercises à importer:</label>
                                <textarea id="import" value={this.state.importValue} onChange={this.handleImportChange} className="form-control" rows={10} />
                            </div>
                            {this.state.responseMsg && this.state.responseSuccess &&
                                <div className="alert alert-info">{this.state.responseMsg}</div>
                            }
                            {this.state.responseMsg && !this.state.responseSuccess &&
                                <div className="alert alert-danger">{this.state.responseMsg}</div>
                            }
                            <div className="form-group">
                                {this.state.sending ? getSmallSpinner() : (<input type="submit" value="Envoyer" className="btn button-animated full-width" />) }
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default AdminQcmImport;