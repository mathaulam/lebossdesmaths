import {appConfig} from "app/tools/AppConfig";

export function findAncestorByClass(el, cls) {
    while (!el.classList.contains(cls) && (el = el.parentElement));
    return el;
}

export function preprocessHtml(html) {
    if (!html) return null;
    
    // In node dev mode, the relative path points to the nodejs port,
    // so it needs to be transformed to an absolute path with django port
    if (appConfig.isNodeServer) {
        html = html.replace(/src="\/media/g, 'src="' + appConfig.getServerUrl() + '/media');
    }

    return html;
}

export function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

export function getErrorMsg(error) {
    let errorMsg = 'Erreur inconnue';
    if (error.response) {
        errorMsg = error.response.data;
    } else {
        errorMsg = error.message;
    }
    return errorMsg;
}
