# -*- coding: utf-8 -*-
from django.shortcuts import redirect
from rest_framework import viewsets
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import TokenAuthentication, \
    SessionAuthentication
from common.helpers import get_token_user, check_admin_token
from cours.helpers import prepare_questions, complete_answered_tags, \
    validate_answers, get_score_result
from cours.models import Cours, Theme, Epreuve, ReponseEpreuve, Programme, Block, Grain, Lesson
from cours.serializers import CoursSerializer, CoursMinimalSerializer, \
    ThemeSerializer, ProgrammeSerializer, BlockSerializer, GrainSerializer, LessonSerializer
from django.http import JsonResponse, HttpResponseBadRequest, \
    HttpResponseForbidden, HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import logging
import json

log = logging.getLogger('django')


class CoursViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows cours to be viewed.
    """
    queryset = Cours.objects.all()
    serializer_class = CoursSerializer

    @list_route()
    def minimal(self, request, *args, **kwargs):
        cours = Cours.objects.all()
        serializer = CoursMinimalSerializer(cours, many=True, context={'request': request})
        return Response(serializer.data)


class LessonViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows classes to be viewed or edited.
    """
    queryset = Lesson.objects.all()
    serializer_class = LessonSerializer

    @detail_route(methods=['get'])
    def grains(self, request, pk=None):
        grains = Grain.objects.filter(lesson_id=pk)
        serializer = GrainSerializer(grains, many=True, context={'request':request})
        return Response(serializer.data)


class ThemeViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows themes to be viewed.
    """
    queryset = Theme.objects.all()
    serializer_class = ThemeSerializer
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)


class GrainsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows grains to be viewed.
    """
    queryset = Grain.objects.all()
    serializer_class = GrainSerializer
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)


class ProgrammesViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API that allow querying the programmes instances.
    """
    queryset = Programme.objects.all()
    serializer_class = ProgrammeSerializer
    permission_classes = (AllowAny,)

    @detail_route(methods=['get'])
    def blocs(self, request, pk=None):
        blocs = Block.objects.filter(programmes=pk)
        serializer = BlockSerializer(blocs, many=True, context={'request':request})
        return Response(serializer.data)


class BlocsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API that allow querying the programmes instances.
    """
    queryset = Block.objects.all()
    serializer_class = BlockSerializer
    permission_classes = (AllowAny,)


@method_decorator(csrf_exempt, name='dispatch')
class EpreuveView(View):

    def get(self, request, *args, **kwargs):

        # Check authorization
        user = get_token_user(request)
        if not user:
            return HttpResponseForbidden("Droits insuffisants")

        try:
            epreuve_id = int(request.GET.get('id', 0))
            epreuve = Epreuve.objects.get(id=epreuve_id)

            eleve_id = int(request.GET.get('eleveId', 0))
            if check_admin_token(request) and eleve_id > 0:
                # Case of admin previewing student result
                epreuve_eleve = ReponseEpreuve.objects.filter(epreuve=epreuve_id, eleve=eleve_id)
            else:
                # Normal case of student getting his own result
                epreuve_eleve = ReponseEpreuve.objects.filter(epreuve=epreuve_id, eleve=user)

            reponses = None
            result = None
            if epreuve_eleve.count() > 0:
                epreuve_reponses = epreuve_eleve[0]
                reponses = epreuve_reponses.reponses

            if reponses:
                # Add student's answers as attribute in tags
                contenu = complete_answered_tags(epreuve.contenu, reponses)

                # Get score result info
                result = get_score_result(reponses)

            else:
                # Strip answer attributes to hide them from user
                contenu = prepare_questions(epreuve.contenu)

            data = {
                'contenu': contenu,
                'cours': epreuve.cours.nom,
                'result': result
            }
            return JsonResponse(data, safe=False)

        except Exception, error:
            response = "Erreur: %s" % str(error)
            log.error(response)
            return HttpResponseBadRequest(response)

    def post(self, request, *args, **kwargs):

        # Check authorization
        user = get_token_user(request)
        if not user:
            return HttpResponseForbidden("Droits insuffisants")

        try:
            epreuve_id = int(request.GET.get('id', 0))
            answers_json_str = request.body
            epreuve = Epreuve.objects.get(pk=epreuve_id)
            found = ReponseEpreuve.objects.filter(epreuve=epreuve)
            if found.count() > 0:
                return HttpResponseForbidden(u"Epreuve déjà faite.")
            else:
                result_json = validate_answers(epreuve.contenu, answers_json_str)
                epreuve_eleve = ReponseEpreuve()
                epreuve_eleve.epreuve = epreuve
                epreuve_eleve.eleve = user
                epreuve_eleve.reponses = json.dumps(result_json, sort_keys=True, ensure_ascii=False).encode('utf8')
                epreuve_eleve.points = 0
                epreuve_eleve.save()
            return HttpResponse()

        except Exception, error:
            response = "Erreur: %s" % str(error)
            log.error(response)
            return HttpResponseBadRequest(response)
