class AppConfig {

    getServerUrl() {
        if (window.location.port == 3000) {
            // Dev mode, use server url as django runserver port 8000
            return "http://localhost:8000";
        } else {
            // Prod mode, server url is the same as client url
            return "http://" + window.location.host;
        }
    }
}

export let appConfig = new AppConfig();
