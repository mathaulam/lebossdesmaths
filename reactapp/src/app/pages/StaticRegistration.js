import React from "react";
import {httpClient} from 'app/tools/HttpClient';
import BasePage from "./BasePage";
import TopNavBar from "app/modules/topbar/TopNavBar";
import StaticRegistrationForm from "app/modules/topbar/StaticRegistrationForm";
import {preprocessHtml, getErrorMsg} from 'app/tools/Helpers.js';
import "app/pages/pages.css";

class StaticRegistration extends BasePage {

    constructor(props) {
        super(props);
        this.state = {class:''};
    }

    renderBody() {

        return (
            <div>
                <section className="title-section">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-sm-8 col-lg-6">
                                <StaticRegistrationForm />
                            </div> 
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default StaticRegistration;