# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.utils.html import strip_tags

from ckeditor_uploader.fields import RichTextUploadingField

from cours.models import Cours, Theme


class Qcm(models.Model):
    theme = models.ForeignKey(Theme)
    niveau = models.PositiveSmallIntegerField()
    serie = models.PositiveSmallIntegerField()
    variante = models.PositiveSmallIntegerField()
    date_import = models.DateTimeField(blank=True, null=True)
    question = RichTextUploadingField(blank=True)
    reponse1 = RichTextUploadingField(blank=True)
    reponse2 = RichTextUploadingField(blank=True)
    reponse3 = RichTextUploadingField(blank=True)
    reponse4 = RichTextUploadingField(blank=True)


    def question_stripped(self):
        return strip_tags(self.question)

    def __unicode__(self):
        return u'Question #%s' % self.id


class ExerciseTypes(models.Model):
    """
    A class that handles the available kinds of exercise to be found.
    """
    # Helpers
    class Meta:
        verbose_name = 'Exercise Type'
        verbose_name_plural = 'Exercise Types'

    # Attributes
    type = models.CharField(max_length=50)

    # Methods
    def __unicode__(self):
        return u'%s' % (self.type)


class GeneralExercise(models.Model):
    """
    An abstract representation for any exercise.
    Should handle any kind of exercise dynamically.
    """
    # Helpers
    difficulties = (
        (1, 'Easy'),
        (2, 'Medium'),
        (3, 'Hard'),
    )

    # Attributes
    grain = models.ForeignKey(
        to='cours.Grain',
        related_name='related_exercises',
        on_delete=models.CASCADE
    )
    type = models.ForeignKey(
        to='qcms.ExerciseTypes',
        related_name='related_exercises',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    ratio = models.FloatField(default=1)
    difficulty = models.PositiveSmallIntegerField(choices=difficulties, default=1)
    niveau = models.PositiveSmallIntegerField(default=0)
    serie = models.PositiveSmallIntegerField(default=0)
    variante = models.PositiveSmallIntegerField()
    date_import = models.DateTimeField(blank=True, null=True)
    question = RichTextUploadingField(blank=True)
    reponse1 = RichTextUploadingField(blank=True, null=True)
    reponse2 = RichTextUploadingField(blank=True, null=True)
    reponse3 = RichTextUploadingField(blank=True, null=True)
    reponse4 = RichTextUploadingField(blank=True, null=True)

    class Meta:
        unique_together = ('grain', 'type', 'serie', 'variante')

    def question_stripped(self):
        return strip_tags(self.question)

    def __unicode__(self):
        return u'Question #%s' % self.id

class QcmSequence(models.Model):
    theme = models.ForeignKey(Theme, null=True)
    nom = models.CharField(max_length=255, null=True, blank=True)
    eleve = models.ForeignKey(User)
    qcm_list = models.CharField(max_length=255)
    mail_envoye = models.BooleanField(default=False)
    score = models.PositiveSmallIntegerField(null=True, blank=True)
    termine = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    expiration = models.DateTimeField(null=True, blank=True)


class Medaille(models.Model):
    eleve = models.ForeignKey(User)
    theme = models.ForeignKey(Theme)
    niveau = models.SmallIntegerField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
